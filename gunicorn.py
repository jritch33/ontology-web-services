import multiprocessing
import os

# bind = '0.0.0.0:' + os.environ.get('PORT', '8000')
bind = '127.0.0.1:' + os.environ.get('PORT', '8000')
workers = multiprocessing.cpu_count() * 2 + 1
# workers = 2
chdir = os.path.dirname(os.path.abspath(__file__)) + '/classification'
timeout = 300
max_requests = 15