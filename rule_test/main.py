import requests
import json
import os
from pprint import pprint
from termcolor import colored
import time
import re

base_url = 'http://localhost:8000/cancer_risk/assessment'

current_milli_time = lambda: int(round(time.time() * 1000))

def time_to_complete_in_sec(start_time):
    return str((current_milli_time() - start_time) / 1000) + 's'

def time_to_complete_in_min(start_time):
    return str(int((current_milli_time() - start_time) / 1000 / 60)) + 'm ' + str(((current_milli_time() - start_time) / 1000) % 60) + 's'

def format_case_name(case_name, org): 
    split = case_name.split('.')
    if org == 'nccn':
        split[0] = split[0].upper()
        split[1] = re.sub('[^0-9]', '', split[1])
    elif org == 'acmg':
        split[0] = split[0].capitalize()
        split[1] = re.sub('[^0-9]', '', split[1])
        
    cn = '.'.join(split).replace('.json', '')
    # print(cn)
    return cn

def run_test(cancer, case, case_name, org):
    start_time = current_milli_time()
    print(colored('testing ' + case_name + '...', 'yellow'), end = ' ')
    url = ''
    if org == 'all': url = base_url
    else: url = base_url + '/' + org

    r = requests.post(url = url, json=case['payload'])
    rjson = r.json()
    
    if 'message' in rjson:
        pprint(rjson)
    else:
        proband = next((g for g in rjson['familyGuidelineAssessment'] if g['relation'] == 'self'), None)
        rules = []
        for rec in proband['recommendations']:
            for trigger in rec['triggers']:
                rules.append(trigger['id'])
        
        evaluate_test(case, case_name, rules, start_time, org)

def evaluate_test(case, case_name, patient_guidelines, start_time, org):
    target = case['target'] if case['target'] == 'ACMGAtRiskFDRPatient' or case_name == 'test.json' else format_case_name(case_name, org)
    if target in set(patient_guidelines):
        print(colored('passed', 'green'), colored(time_to_complete_in_sec(start_time), 'blue'), patient_guidelines)
    else:
        print(colored('failed', 'red'), colored(time_to_complete_in_sec(start_time), 'blue'), patient_guidelines)


def run_test_suite(cancer, org):
    test_path = os.path.dirname(os.path.abspath(__file__)) + '/data/' + org + '/' + cancer
    tests = os.listdir(test_path)

    for test in tests:
        with open(os.path.join(test_path, test), 'r') as t:
            run_test(cancer, json.loads(t.read()), test, org)

def execute():
    start_time = current_milli_time()

    #  runs all tests for both CPGs
    run_test_suite('acmg_test', 'acmg')
    run_test_suite('nccn_test', 'nccn')
    
    #  runs tests for individual CPGs by organization
    # run_test_suite('brain', 'acmg')
    # run_test_suite('breast', 'acmg')
    # run_test_suite('colorectal', 'acmg')
    # run_test_suite('endometrial', 'acmg')
    # run_test_suite('gastric', 'acmg')
    # run_test_suite('leukemia', 'acmg')
    # run_test_suite('melanoma', 'acmg')
    # run_test_suite('ovarian', 'acmg')
    # run_test_suite('pancreatic', 'acmg')
    # run_test_suite('prostate', 'acmg')
    # run_test_suite('renal', 'acmg')
    # run_test_suite('sarcoma', 'acmg')
    # run_test_suite('thyroid', 'acmg')

    # run_test_suite('breast_ovarian_pancreatic', 'nccn')
    # run_test_suite('colorectal', 'nccn')

    print(colored('Tests completed in ' + time_to_complete_in_min(start_time), 'blue'))

if __name__ == '__main__':
    execute()