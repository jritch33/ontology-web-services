# README #

### Create and apply ontology  ###

* This repo contains two APIs -- the ontology definition API to create OWL ontologies and the classification API that loads a hereditary cancer ontology and applies it to patient data. 
* version 1.0.0

### API Documenataion ###

* [Ontology Definition API](http://backend1.itruns.in:7060/swagger/)
* [Classification API](http://backend1.itruns.in:7080/swagger/)

### Contact ###

	Jordon Ritchie
	ritchiej@musc.edu