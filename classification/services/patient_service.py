from owlready2 import *
from services.cancer_service import CancerService
from services.gene_service import GeneService
from services.polyp_service import PolypService
from services.risk_model_service import RiskModelService
from oao.oao import Oao
from pprint import pprint
import sys
import time
import uuid
import json

class PatientService:
    def __init__(self, ontology_service): 
        self.ontology_service = ontology_service
        self.onto = self.ontology_service.get_onto()
        self.cancer_service = CancerService(ontology_service)
        self.gene_service = GeneService(ontology_service)
        self.polyp_service = PolypService(ontology_service)
        self.risk_model_service = RiskModelService(ontology_service)
        self.oao = self.ontology_service.oao
        self.fhir_codes = {
            'parent': ['FTH', 'MTH'],
            'child': ['SON', 'DAU'],
            'sibling': ['SIS', 'BRO'],
            'parent_sibling': ['MAUNT', 'PAUNT', 'MUNCLE', 'PUNCLE'],
            'sibling_child': [ 'NIECE', 'NEPHEW'],
            'grandparent': ['MGRMTH', 'MGRFTH', 'PGRMTH', 'PGRFTH'],
            'grandchild': ['GRNDSON', 'GRNDDAU']
        }
        self.display_relations = {
            'self': 'self',
            'MTH': 'Mother',
            'FTH': 'Father',
            'BRO': 'Brother',
            'SIS': 'Sister',
            'SON': 'Son',
            'DAU': 'Daughter',
            'MAUNT': 'Maternal Aunt',
            'PAUNT': 'Paternal Aunt',
            'MUNCLE': 'Maternal Uncle',
            'PUNCLE': 'Paternal Uncle',
            'NIECE': 'Neice',
            'NEPHEW': 'Nephew',
            'MGRMTH': 'Maternal Grandmother',
            'PGRMTH': 'Paternal Grandmother',
            'MGRFTH': 'Maternal Grandfather',
            'PGRFTH': 'Paternal Grandfather',
            'GSON': 'Grandson',
            'GDAUC': 'Granddaughter'
        }

    def create_patient_name(self, index, relation_to_proband):
        return 'Patient' + str(index) + '_' + relation_to_proband + '_of_Patient0'

    def create_patient(self, index, relation_to_proband):
        with self.onto:
            if relation_to_proband == 'self': 
                patient = self.onto.Patient(name = 'Patient' + str(index))  
                patient.relation_to_proband.append('self')
            else: 
                patient = self.onto.Patient(name = self.create_patient_name(index, relation_to_proband))
                patient.relation_to_proband.append(relation_to_proband)
            
            return patient

    def add_patient_to_ontology(self, index, relation_to_proband, data):
        patient = self.create_patient(index, relation_to_proband)
        patient.add_patient_name(data)
        patient.add_age_sex(data, self.onto)
        patient.add_deceased(data, self.onto)
        if 'conditions' in data: patient.add_conditions(data, self.cancer_service, self.polyp_service)
        if 'traits' in data: 
            traits = data['traits']
            if 'ancestry' in traits: 
                for a in traits['ancestry']:
                    ancestry = a.replace('-', '_')
                    if ancestry in [anc.name for anc in self.oao.get_ancestry_instances()]:
                        patient.add_ancestry(self.oao.get_ancestry_instance(ancestry))
            if 'skinColor' in traits:
                if traits['skinColor'] in [sk.name for sk in self.oao.get_skin_color_instances()]:
                    patient.add_skin_color(self.oao.get_skin_color_instance(traits['skinColor']))
            if 'bodyWeight' in traits or 'bodyHeight' in traits:
                biometrics = {}
                if 'bodyWeight' in traits:
                    biometrics['bodyWeight'] = traits['bodyWeight']
                if'bodyHeight' in traits:
                    biometrics['bodyHeight'] = traits['bodyHeight']
                patient.add_biometrics(biometrics)
        if 'geneMutations' in data:
            patient.add_gene_mutations(data, self.gene_service)
        if 'colonoscopy' in data:
            patient.add_colonoscopy(data)
        if relation_to_proband == 'self':
            patient.add_mammogram(data)
            patient.add_smoking_history(data)
            patient.add_drinking_history(data)
            patient.add_menstruation_history(data)
            patient.add_menopause_history(data)
        return patient

    def set_patient_relations(self, data, family):
        proband = family['proband']
        for r1 in family['relatives']:
            rel1 = r1['patient']
            for r2 in family['relatives']:
                rel2 = r2['patient']
                if rel1.name != rel2.name:
                    #  Proband
                    if r1['relation_to_proband'] == 'self':
                        is_fdr = True if r2['relation_to_proband'] in self.fhir_codes['parent'] + self.fhir_codes['child'] + self.fhir_codes['sibling'] else False
                        rel1.relate(rel2, r2['relation_to_proband'], self.onto, is_fdr)
                    #  Proband's children
                    #  Proband's children are siblings
                    if r1['relation_to_proband'] in self.fhir_codes['child'] and r2['relation_to_proband'] in self.fhir_codes['child']:
                        if r2['relation_to_proband'] == 'DAU': rel1.relate(rel2, 'SIS', self.onto, True)
                        elif r2['relation_to_proband'] == 'SON': rel1.relate(rel2, 'BRO', self.onto, True)
                    #  Sibling of proband is aunt or uncle of proband's child
                    if r1['relation_to_proband'] in self.fhir_codes['child'] and r2['relation_to_proband'] in self.fhir_codes['sibling']:
                        if self.onto.female in proband.has_sex:
                            if r2['relation_to_proband'] == 'SIS':
                                rel1.relate(rel2, 'MAUNT', self.onto, False)
                            elif r2['relation_to_proband'] == 'BRO':
                                rel1.relate(rel2, 'MUNCLE', self.onto, False)
                        if self.onto.male in proband.has_sex:
                            if r2['relation_to_proband'] == 'SIS':
                                rel1.relate(rel2, 'PAUNT', self.onto, False)
                            elif r2['relation_to_proband'] == 'BRO':
                                rel1.relate(rel2, 'PUNCLE', self.onto, False)

                    #  Proband child to proband parent is grandparent - grandchild relation
                    if r1['relation_to_proband'] in self.fhir_codes['child'] and r2['relation_to_proband'] in self.fhir_codes['parent']:
                        if self.onto.female in proband.has_sex:
                            if r2['relation_to_proband'] == 'MTH':
                                rel1.relate(rel2, 'MGRMTH', self.onto, False)
                            elif r2['relation_to_proband'] == 'FTH':
                                rel1.relate(rel2, 'MGRFTH', self.onto, False)
                        if self.onto.male in proband.has_sex:
                            if r2['relation_to_proband'] == 'MTH':
                                rel1.relate(rel2, 'PGRMTH', self.onto, False)
                            elif r2['relation_to_proband'] == 'FTH':
                                rel1.relate(rel2, 'PGRFTH', self.onto, False)

                    #  Nieces and Nephews
                    #  Proband sibling has same relationship with nieces and nephews
                    #  Ignoring Nieces and Nephews of proband for the time being.
                    #  Currently cannot determine which sibling the kids belong to based on 'relationToProband' attribute. 
                    #  This might require restructuring.
                    if r1['relation_to_proband'] in self.fhir_codes['sibling'] and r2['relation_to_proband'] in self.fhir_codes['sibling_child']:
                        rel1.relate(rel2, r2['relation_to_proband'], self.onto, False)

                    #  Sibling
                    #  Siblings to proband are also siblings to each other
                    if r1['relation_to_proband'] in self.fhir_codes['sibling'] and r2['relation_to_proband'] in self.fhir_codes['sibling']:
                        rel1.relate(rel2, r2['relation_to_proband'], self.onto, True)

                    #  Proband sibling has same parents 
                    if r1['relation_to_proband'] in self.fhir_codes['sibling'] and r2['relation_to_proband'] in self.fhir_codes['parent']:
                        rel1.relate(rel2, r2['relation_to_proband'], self.onto, True)
                        # rel1.set_parent_child_relations(rel2, r2['relation_to_proband'], self.onto)  

                    #  Proband sibling has same aunts and uncles                                    
                    if r1['relation_to_proband'] in self.fhir_codes['sibling'] and r2['relation_to_proband'] in self.fhir_codes['parent_sibling']:
                        rel1.relate(rel2, r2['relation_to_proband'], self.onto, False)
                    
                    #  Proband sibling has same grandparents
                    if r1['relation_to_proband'] in self.fhir_codes['sibling'] and r2['relation_to_proband'] in self.fhir_codes['grandparent']:
                        rel1.relate(rel2, r2['relation_to_proband'], self.onto, False)

                    #  Parent
                    #  Aunt or Uncle of proband is parent's sibling
                    if r1['relation_to_proband'] in self.fhir_codes['parent'] and r2['relation_to_proband'] in self.fhir_codes['parent_sibling']:
                        if r1['relation_to_proband'] == 'MTH':
                            if r2['relation_to_proband'][0:1] == 'M': 
                                if self.onto.female in rel2.has_sex:
                                    rel1.relate(rel2, 'SIS', self.onto, True)
                                elif self.onto.male in rel2.has_sex:
                                    rel1.relate(rel2, 'BRO', self.onto, True)
                        elif r1['relation_to_proband'] == 'FTH':
                            if r2['relation_to_proband'][0:1] == 'P': 
                                if self.onto.female in rel2.has_sex:
                                    rel1.relate(rel2, 'SIS', self.onto, True)
                                elif self.onto.male in rel2.has_sex:
                                    rel1.relate(rel2, 'BRO', self.onto, True)

                    #  Parent Siblings
                    #  Aunts an Uncles on same side are siblings
                    if r1['relation_to_proband'] in self.fhir_codes['parent_sibling'] and r2['relation_to_proband'] in self.fhir_codes['parent_sibling']:
                        if r1['relation_to_proband'] == 'MAUNT':
                            if r2['relation_to_proband'] == 'MUNCLE': 
                                rel1.relate(rel2, 'BRO', self.onto, True)
                            elif r2['relation_to_proband'] == 'MAUNT':
                                rel1.relate(rel2, 'SIS', self.onto, True)
                                
                        elif r1['relation_to_proband'] == 'PAUNT':
                            if r2['relation_to_proband'] == 'PUNCLE': 
                                rel1.relate(rel2, 'BRO', self.onto, True)
                            elif r2['relation_to_proband'] == 'PAUNT':
                                rel1.relate(rel2, 'SIS', self.onto, True)

                    #  Proband grandparent is parent to probands parent
                    if r1['relation_to_proband'] in self.fhir_codes['parent'] and r2['relation_to_proband'] in self.fhir_codes['grandparent']:
                        if r1['relation_to_proband'] == 'MTH' and r2['relation_to_proband'] == 'MGRMTH':
                            rel1.relate(rel2, 'MTH', self.onto, True)
                        elif r1['relation_to_proband'] == 'MTH' and r2['relation_to_proband'] == 'MGRFTH':
                            rel1.relate(rel2, 'FTH', self.onto, True)
                        elif r1['relation_to_proband'] == 'FTH' and r2['relation_to_proband'] == 'PGRMTH':
                            rel1.relate(rel2, 'MTH', self.onto, True)
                        elif r1['relation_to_proband'] == 'FTH' and r2['relation_to_proband'] == 'PGRFTH':
                            rel1.relate(rel2, 'FTH', self.onto, True)

                    #  Proband grandparent is parent to probands aunt and uncle
                    if r1['relation_to_proband'] in self.fhir_codes['parent_sibling'] and r2['relation_to_proband'] in self.fhir_codes['grandparent']:
                        if r1['relation_to_proband'] in ['MAUNT', 'MUNCLE'] and r2['relation_to_proband'] == 'MGRMTH':
                            rel1.relate(rel2, 'MTH', self.onto, True)
                        elif r1['relation_to_proband'] in ['MAUNT', 'MUNCLE'] and r2['relation_to_proband'] == 'MGRFTH':
                            rel1.relate(rel2, 'FTH', self.onto, True)
                        elif r1['relation_to_proband'] in ['PAUNT', 'PUNCLE'] and r2['relation_to_proband'] == 'PGRMTH':
                            rel1.relate(rel2, 'MTH', self.onto, True)
                        elif r1['relation_to_proband'] in ['PAUNT', 'PUNCLE'] and r2['relation_to_proband'] == 'PGRFTH':
                            rel1.relate(rel2, 'FTH', self.onto, True)
                    
    def add_family_to_ontology(self, data):
        unaffected_rel_counts = {}
        unaffected_rel = []
        if 'relatives' in data: affected_rel = data['relatives'] 
        else: affected_rel = []

        #  How many unaffected relatives to add
        if 'pedigree' in data:
            unaffected_rel_counts['SON'] = data['pedigree']['sons']
            unaffected_rel_counts['DAU'] = data['pedigree']['daughters']
            unaffected_rel_counts['BRO'] = data['pedigree']['brothers']
            unaffected_rel_counts['SIS'] = data['pedigree']['sisters']
            unaffected_rel_counts['MAUNT'] = data['pedigree']['maternalAunts']
            unaffected_rel_counts['MUNCLE'] = data['pedigree']['maternalUncles']
            unaffected_rel_counts['PAUNT'] = data['pedigree']['paternalAunts']
            unaffected_rel_counts['PUNCLE'] = data['pedigree']['paternalUncles']

            #  Everyone has these
            unaffected_rel_counts['FTH'] = 1
            unaffected_rel_counts['MTH'] = 1
            unaffected_rel_counts['PGRFTH'] = 1
            unaffected_rel_counts['PGRMTH'] = 1
            unaffected_rel_counts['MGRFTH'] = 1
            unaffected_rel_counts['MGRMTH'] = 1
        
        #  Don't count affected relatives twice
        for r in affected_rel:
            if r['relationToProband'] in unaffected_rel_counts: unaffected_rel_counts[r['relationToProband']] -= 1

        #  Create trimmed list of affected relatives with virtually null data
        #  (User doesn't provide any info unless relative has cancer)
        for rel_key in unaffected_rel_counts:
            index = 1
            while index <= unaffected_rel_counts[rel_key]:
                sex = 'male' if rel_key in ['FTH', 'SON', 'BRO', 'MUNCLE', 'PUNCLE', 'PGRFTH', 'MGRFTH'] else 'female'
                unaffected_rel.append({
                    'relationToProband': rel_key,
                    'sex': sex,
                    'conditions': [],
                    'relatives': [],
                    'traits': {},
                    'geneMutations': [],
                    'smokingHistory': {},
                    'drinkingHistory': {}
                })

                index += 1


        #  Add unaffected relatives to relatives
        if 'relatives' in data: data['relatives'] = data['relatives'] + unaffected_rel
        else: data['relatives'] = unaffected_rel

        proband = self.add_patient_to_ontology(0, data['relationToProband'], data)
        family = {
            'proband': proband,
            'relatives': [
                {
                    'relation_to_proband': data['relationToProband'],
                    'patient': proband,
                    'data': data
                }
            ]
        }
        
        if 'relatives' in data:
            index = 1
            for r in data['relatives']:
                family['relatives'].append({
                    'relation_to_proband': r['relationToProband'],
                    'patient': self.add_patient_to_ontology(str(index), r['relationToProband'], r),
                    'data': r
                })
                index += 1
            
            self.set_patient_relations(data, family)
    
        self.set_all_patients_disjoint()
        for ind in self.onto.Patient.instances(): close_world(ind)
        return family

    def set_all_patients_disjoint(self):
        if self.onto.Patient.instances(): AllDifferent([p for p in self.onto.Patient.instances()]) 

    def close_individuals_for_property(self, prop, individuals):
        for ind in individuals: 
            if prop[ind]:
                ind.is_a.append(prop.only(OneOf(prop[ind])))
            else:
                ind.is_a.append(prop.only(Nothing))

    def delete_patient(self, name):
        for i in list(self.onto.individuals()):
            if name in i.name:
                destroy_entity(i)

    def run_guidelines(self, onto_family):
        # self.ontology_service.sync()
        self.ontology_service.execute_rule_engine()
            
        # self.ontology_service.save()

        family_guideline_assessment = []
        for member in onto_family['relatives']:
            onto_member = member['patient']
            
            onto_member_obj = {
                'name': onto_member.get_patient_name(),
                'ontoName': onto_member.name,
                'sex': onto_member.get_sex(),
                'relation': onto_member.get_relation_to_proband(),
                'displayRelation': self.display_relations[onto_member.get_relation_to_proband()],
                'recommendations': onto_member.get_patient_recommendations()
            }
            
            if onto_member.get_patient_age():
                onto_member_obj['patientAge'] = onto_member.get_patient_age()

            family_guideline_assessment.append(onto_member_obj)

        return family_guideline_assessment

    def assess_patient_risk(self, data):
        print('Assessing patient risk...')
        print('Adding family to ontology...')
        onto_family = self.add_family_to_ontology(data)
        print('Family added to ontology.')
        proband = onto_family['proband']
        print('Running guideline assessment...')
        guideline_assessment = self.run_guidelines(onto_family)
        print('Guideline assessment completed.')
        proband_guideline_assessment = next(member for member in guideline_assessment if member["relation"] == 'self')
        family_members_with_cancer = self.get_family_members_with_cancer(onto_family, guideline_assessment)
        risk_assessment = {
            'patientName': proband.get_patient_name(),
            'patientFirstName': proband.get_patient_first_name(),
            'patientAge': proband.get_patient_age(),
            'patientSex': proband.get_sex(),
            'patientAncestry': self.get_ancestry_info(proband),
            'patientCancer': proband.is_cancer_patient(),
            'patientCancers': self.get_cancer_info(proband),
            'patientGeneMutations': self.get_patient_gene_mutation_info(proband),
            'patientBiometrics': {
                'bmi': proband.get_bmi(),
                'bmiCategory': self.get_bmi_category(proband.get_bmi()),
                'height': proband.get_height() if proband.get_height() else 0,
                'weight': proband.get_weight() if proband.get_weight() else 0
            },
            'patientSmokingHistory': {
                'currentSmoker': proband.get_years_since_quite_smoking() == 0,
                'ageQuit': proband.get_patient_age() - proband.get_years_since_quite_smoking(),
                'packsPerDay': proband.get_packs_smoked_per_day(),
                'packYearsSmoked': proband.has_pack_years_smoked,
                'smokingFrequency': proband.smoking_frequency[0]
            },
            'patientAlcoholHistory':{
                'drinkingFrequency': proband.drinking_frequency[0] if proband.drinking_frequency else 'never'
            },
            'patientMenstrualHistory': {
                'ageMenstruation': proband.get_patient_age_menstruation(),
                'ageMenopause': proband.get_patient_age_menopause(),
                'isPremenopausal': proband.is_premenopausal()
            },
            'patientScreeningHistory': {},
            'patientExecutiveSummary': self.get_executive_summary(proband, family_members_with_cancer),
            'patientSyndromeInfo': self.get_syndrome_info(proband, proband_guideline_assessment, guideline_assessment),
            'geneMutationPresentInFamily': proband.hereditary_mutation_is_present(),
            'geneMutationPresentInPatient': proband.mutation_is_present(),
            'hasAshkenaziAncestry': True if 'Ashkenazi' in self.get_ancestry_info(proband) else False,
            'familyGeneMutations': self.get_family_gene_mutations(proband),
            'familyMembersWithCancer': family_members_with_cancer,
            'familyCancerStats': self.get_family_cancer_stats(onto_family),
            'familyMembersByCancerType': self.get_family_members_by_cancer_type(family_members_with_cancer),
            'patientRecommendations': self.get_recommendation_criteria(proband_guideline_assessment),
            'familyGuidelineAssessment': guideline_assessment,
            'familyMembersUnaffectedAtRisk': self.get_unaffected_family_members_at_risk(onto_family, guideline_assessment),
            'hereditaryRisk': proband.is_hereditary_cancer_risk_patient(guideline_assessment)
        }

        if proband.get_months_since_last_mammogram() is not None: risk_assessment['patientScreeningHistory']['monthsSinceLastMammogram'] = proband.get_months_since_last_mammogram()
        if proband.get_patient_age_last_colonoscopy() is not None: risk_assessment['patientScreeningHistory']['ageLastColonoscopy'] = proband.get_patient_age_last_colonoscopy()
        if proband.get_pj_polyp_count() is not None: risk_assessment['patientScreeningHistory']['pjPolypCount'] = proband.get_pj_polyp_count()
        if proband.get_juvenile_polyp_count() is not None: risk_assessment['patientScreeningHistory']['juvenilePolypCount'] = proband.get_juvenile_polyp_count()
        if proband.get_hamartomatous_polyp_count() is not None: risk_assessment['patientScreeningHistory']['hamartomatousPolypCount'] = proband.get_hamartomatous_polyp_count()
        if proband.get_adenomatous_polyp_count() is not None: risk_assessment['patientScreeningHistory']['adenomatousPolypCount'] = proband.get_adenomatous_polyp_count()

        return risk_assessment

    def get_executive_summary(self, proband, family_members_with_cancer):
        summary = []
        if proband.is_cancer_patient():
            cancers =[]
            for c in self.get_cancer_info(proband):
                if 'ageOnset' in c: cancers.append(c['name'] + ' at ' + str(c['ageOnset']) + ' years old')
                else: cancers.append(c['name'])
            summary.append('Personal history of ' + ', '.join(cancers))
        else: summary.append('No reported personal history of cancer')

        ancestry_info = self.get_ancestry_info(proband)
        if ancestry_info:
            if 'Ashkenazi' in ancestry_info: summary.append('Askenazi Jewish ancestry')
            else: summary.append(', '.join(ancestry_info) + ' ancestry')

        if self.get_patient_gene_mutation_info(proband):
            gene_muts = []
            for g in self.get_patient_gene_mutation_info(proband):
                if g['gene'] == 'Unspecified': gene_muts.append('an unspecified gene')
                else: gene_muts.append(g['gene'])
            summary.append('Positive pathogenic mutation in ' + ', '.join(gene_muts))
        else: summary.append('No reported personal gene mutation')

        mut_in_fam = False
        for member in family_members_with_cancer:
            if member['patientGeneMutations']:
                gene_muts = []
                for g in member['patientGeneMutations']:
                    if g['gene'] == 'Unspecified': gene_muts.append('an unspecified gene')
                    else: gene_muts.append(g['gene'])
                summary.append(member['displayRelationToProband'] + ' with a positive pathogenic mutation in ' + ', '.join(gene_muts))
                mut_in_fam = True
        if not mut_in_fam : summary.append('No reported family gene mutation history')                
        
        if self.get_cancers_in_the_family(family_members_with_cancer):
            family_cancers = self.get_cancers_in_the_family(family_members_with_cancer)
            summary.append('Family history of ' + ', '.join(family_cancers))
        else: summary.append('No family history of cancer')

        return summary

    def get_ancestry_info(self, proband):
        return [anc.display[0] for anc in proband.get_ancestry()]

    def get_patient_gene_mutation_info(self, proband):
        mutations = []
        for mut in proband.get_gene_mutations():
            mutations.append({
                'gene': mut.get_class().display[0],
            })
        return mutations

    def get_family_gene_mutations(self, proband):
        genes = []
        for member in proband.get_relatives():
            member_gene_list = []
            for mut in member.get_gene_mutations():
                if mut.get_class().display:
                    if 'Unspecified' in mut.get_class().display: 
                        member_gene_list.append('an unspecified gene' )
                    else:
                        member_gene_list.append(mut.get_class().display[0])
            genes = genes + member_gene_list
        return list(set(genes))

    def get_cancer_info(self, proband):
        cancers = []
        for cancer in proband.get_cancers():
            cancer_obj = {
                'name': cancer.get_cancer_display_name(),
                'attributes': cancer.get_hormone_statuses() + cancer.get_laterality() + cancer.get_cancer_tissue_origin() + cancer.get_histology() + cancer.get_factors(),
                'relatedMutations': [ mut.display[0] for mut in proband.get_gene_mutations() if cancer.name in mut.__class__.mutation_increases_risk_for ]
            }

            if cancer.get_age_onset():
                cancer_obj['ageOnset'] = cancer.get_age_onset()

            if 'Prostate' in cancer.name:
                gleason = "Gleason score: " + str(cancer.get_gleason_score())
                cancer_obj['attributes'] += cancer_obj['attributes'] + [gleason]
                
            cancers.append(cancer_obj)
        return cancers

    def get_family_members_with_cancer(self, onto_family, guideline_assessment):
        relatives_with_cancer = []
        for member in onto_family['relatives']:
            member_patient = member['patient']
            patient_cancers = self.get_cancer_info(member_patient)
            patient_gene_mutations = self.get_patient_gene_mutation_info(member_patient)
            if patient_cancers and member_patient.get_relation_to_proband() != 'self': 
                rel = {
                    'patientName': member_patient.get_patient_name(),
                    'patientIsDeceased': member_patient.is_deceased_patient(),
                    'relationToProband': member_patient.get_relation_to_proband(),
                    'displayRelationToProband': self.display_relations[member_patient.get_relation_to_proband()],
                    'patientCancers': patient_cancers,
                    'patientGeneMutations': patient_gene_mutations,
                    'referForGeneticCounseling': self.patient_meets_criteria(member_patient, guideline_assessment)
                }
                
                if member_patient.get_patient_age():
                    rel['patientAge'] = member_patient.get_patient_age()

                if member_patient.is_deceased_patient():
                    if member_patient.get_patient_age_of_death():
                        rel['patientAgeOfDeath'] = member_patient.get_patient_age_of_death()
                    if member_patient.get_patient_year_of_death():
                        rel['patientYearOfDeath'] = member_patient.get_patient_year_of_death()
                
                relatives_with_cancer.append(rel)

        return relatives_with_cancer

    def patient_meets_criteria(self, patient, guideline_assessment):
        for assessment in guideline_assessment:
            if assessment['relation'] == patient.get_relation_to_proband():
                if assessment['recommendations']: return True
        return False

    def get_unaffected_family_members_at_risk(self, onto_family, guideline_assessment):
        unaffected_at_risk_family_members = []
        for member in onto_family['relatives']:
            member_patient = member['patient']
            if member_patient.get_relation_to_proband() != 'self':
                if self.get_unaffected_family_member_acmg_risk(guideline_assessment, member_patient):
                    unaffected_at_risk_family_members.append({
                        'patientName': member_patient.get_patient_name(),
                        'ontoName': member_patient.name,
                        'patientAge': member_patient.get_patient_age(),
                        'relationToProband': member_patient.get_relation_to_proband(),
                        'displayRelationToProband': self.display_relations[member_patient.get_relation_to_proband()],
                    })

        return unaffected_at_risk_family_members

    def get_unaffected_family_member_acmg_risk(self, guideline_assessment, member_patient):
        for assessment in guideline_assessment:
            if assessment['relation'] == member_patient.get_relation_to_proband():
                if assessment['recommendations'] and not member_patient.get_cancers():
                    return True
        
        return False
                    

    def get_family_cancer_stats(self, onto_family):
        stats = {
            'fdr': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 },
            'maternal_grandparents': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 },
            'maternal_aunts': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 },
            'maternal_uncles': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 },
            'maternal_totals': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 },
            'paternal_grandparents': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 },
            'paternal_aunts': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 },
            'paternal_uncles': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 },
            'paternal_totals': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 },
            'parent': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 },
            'child': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 },
            'sibling': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 },
            'total': { 'with_cancer': 0.0, 'total': 0.0, 'perc_with_cancer': 0.0 }
        }

        for member in onto_family['relatives']:
            for code in self.fhir_codes:
                if member['patient'].get_relation_to_proband() in self.fhir_codes[code]:
                    if member['patient'].get_cancers(): 
                        stats['total']['with_cancer'] += 1
                        stats['total']['total'] += 1
                        if code in ['parent', 'child', 'sibling']: 
                            stats[code]['with_cancer'] += 1
                            stats[code]['total'] += 1
                            stats['fdr']['with_cancer'] += 1
                            stats['fdr']['total'] += 1
                        elif code in ['parent_sibling', 'sibling_child', 'grandparent', 'grandchild']:
                            if member['patient'].get_side_of_the_family() == 'maternal':
                                stats['maternal_totals']['with_cancer'] += 1
                                stats['maternal_totals']['total'] += 1
                                if member['patient'].get_relation_to_proband() in self.fhir_codes['grandparent']:
                                    stats['maternal_grandparents']['with_cancer'] += 1
                                    stats['maternal_grandparents']['total'] += 1
                                elif member['patient'].get_relation_to_proband() in ['MAUNT']:
                                    stats['maternal_aunts']['with_cancer'] += 1
                                    stats['maternal_aunts']['total'] += 1
                                elif member['patient'].get_relation_to_proband() in ['MUNCLE']:
                                    stats['maternal_uncles']['with_cancer'] += 1
                                    stats['maternal_uncles']['total'] += 1
                            elif member['patient'].get_side_of_the_family() == 'paternal':
                                stats['paternal_totals']['with_cancer'] += 1
                                stats['paternal_totals']['total'] += 1
                                if member['patient'].get_relation_to_proband() in self.fhir_codes['grandparent']:
                                    stats['paternal_grandparents']['with_cancer'] += 1
                                    stats['paternal_grandparents']['total'] += 1
                                elif member['patient'].get_relation_to_proband() in ['PAUNT']:
                                    stats['paternal_aunts']['with_cancer'] += 1
                                    stats['paternal_aunts']['total'] += 1
                                elif member['patient'].get_relation_to_proband() in ['PUNCLE']:
                                    stats['paternal_uncles']['with_cancer'] += 1
                                    stats['paternal_uncles']['total'] += 1
                    else: 
                        stats['total']['total'] += 1
                        if code in ['parent', 'child', 'sibling']: 
                            stats[code]['total'] += 1
                            stats['fdr']['total'] += 1
                        elif code in ['parent_sibling', 'sibling_child', 'grandparent', 'grandchild']:
                            if member['patient'].get_side_of_the_family() == 'maternal':
                                stats['maternal_totals']['total'] += 1
                                if member['patient'].get_relation_to_proband() in self.fhir_codes['grandparent']:
                                    stats['maternal_grandparents']['total'] += 1
                                elif member['patient'].get_relation_to_proband() in ['MAUNT']:
                                    stats['maternal_aunts']['total'] += 1
                                elif member['patient'].get_relation_to_proband() in ['MUNCLE']:
                                    stats['maternal_uncles']['total'] += 1
                            elif member['patient'].get_side_of_the_family() == 'paternal':
                                stats['paternal_totals']['total'] += 1
                                if member['patient'].get_relation_to_proband() in self.fhir_codes['grandparent']:
                                    stats['paternal_grandparents']['total'] += 1
                                elif member['patient'].get_relation_to_proband() in ['PAUNT']:
                                    stats['paternal_aunts']['total'] += 1
                                elif member['patient'].get_relation_to_proband() in ['PUNCLE']:
                                    stats['paternal_uncles']['total'] += 1

        for rel in stats:
            stats[rel]['perc_with_cancer'] = round(stats[rel]['with_cancer'] / stats[rel]['total'] * 100, 1) if stats[rel]['total'] != 0 else 0.0

        return stats

    def get_cancers_in_the_family(self, family_members_with_cancer):
        cancers_in_the_family = []
        for member in family_members_with_cancer:
            for cancer in member['patientCancers']:
                cancers_in_the_family.append(cancer['name'])
        cancers_in_the_family = list(set(cancers_in_the_family))
        return cancers_in_the_family

    def get_family_members_by_cancer_type(self, family_members_with_cancer):
        cancers_in_the_family = self.get_cancers_in_the_family(family_members_with_cancer)

        family_members_by_cancer_type = {}
        for cancer in cancers_in_the_family:
            for member in family_members_with_cancer:
                patient_cancers = [c['name'] for c in member['patientCancers']]
                if cancer in patient_cancers:
                    if cancer not in family_members_by_cancer_type: family_members_by_cancer_type[cancer] = [] 

                    family_member_by_cancer_type = {
                        'displayRelationToProband': member['displayRelationToProband']
                    }

                    for c in member['patientCancers']:
                        if c['name'] == cancer and 'ageOnset' in c:
                            family_member_by_cancer_type['ageOnset'] = c['ageOnset']

                    family_members_by_cancer_type[cancer].append(family_member_by_cancer_type)

        return family_members_by_cancer_type

    def get_bmi_category(self, bmi):
        if float(bmi) < 18.5: return 'Underweight'
        elif float(bmi) >= 18.5 and float(bmi) < 25: return 'Normal'
        elif float(bmi) >= 25 and float(bmi) < 30: return 'Overweight'
        elif float(bmi) >= 30: return 'Obese'

    def get_recommendation_criteria(self, proband_guideline_assessment):
        criteria = {}
        if proband_guideline_assessment['recommendations']:
            for guideline in proband_guideline_assessment['recommendations']:
                if guideline['id'] not in criteria:
                    criteria[guideline['id']] = {
                        'text': [ guideline['text'] ],
                        'criteria': []
                    }
                if guideline['text'] not in criteria[guideline['id']]['text']:
                    criteria[guideline['id']]['text'].append(guideline['text'])
                
                crit = criteria[guideline['id']]['criteria'] 
                trigger_crit = [ trigger['criteria'] for trigger in guideline['triggers'] ]
                criteria[guideline['id']]['criteria'] = list(set(crit + trigger_crit))

        return criteria

    def get_acmg_recommendations(self, proband_guideline_assessment):
        proband_recomendations = [guideline['recommendation'] for guideline in proband_guideline_assessment['guidelineRiskAssessment']['acmg_recommendations']]
        return proband_recomendations

    def get_syndrome_info(self, proband, proband_guideline_assessment, guideline_assessment):
        syn_info = {}

        for patient in guideline_assessment:
            for rec in patient['recommendations']:
                for trigger in rec['triggers']:
                    if 'syndrome' in trigger:

                        for syndrome in trigger['syndrome']:
                            onto_syn = self.onto[syndrome]
                            display = onto_syn.display[0]
                            
                            if display not in syn_info:
                                syn_info[display] = {
                                    'description': '',
                                    'indicators': [],
                                }   

                            syn_info[display]['description'] = onto_syn.description[0]

                            if trigger['criteria'] not in syn_info[display]['indicators']:
                                syn_info[display]['indicators'].append(trigger['criteria'])
        
        return syn_info