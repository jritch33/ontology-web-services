from owlready2 import *


class CancerService:
    def __init__(self, ontology_service): 
        self.ontology_service = ontology_service
        self.onto = self.ontology_service.get_onto()
        self.cancer_dict = self.__cancer_dict_init()
    
    def __cancer_dict_init(self):
        cancer_dict = {}
        cancers = self.onto.search(is_a = self.onto.Cancer, is_core_concept = True)
        for c in cancers:
            cancer_dict[c.id[0]] = {
                'cancer_name': c.display[0].replace(' ', '_'),
                'cancer_type': c.name
            }
        return cancer_dict

    def get_cancer_dict(self):
        return self.cancer_dict

    def create_cancer(self, condition, condition_count, patient_name):
        cancer_type = self.cancer_dict[condition['conceptId']]['cancer_type']
        cancer_name = self.cancer_dict[condition['conceptId']]['cancer_name']
        cancer = self.onto[cancer_type](name = '_'.join((patient_name, cancer_name, condition_count)))
        cancer.display.append(cancer_name.replace('_', ' '))

        return cancer

    def add_cancer_to_ontology(self, condition, condition_count, factors, patient_name):
        cancer = self.create_cancer(condition, condition_count, patient_name)

        if 'ageOnset' in condition: cancer.has_age_onset = condition['ageOnset']
    
        if 'laterality' in factors:
            if factors["laterality"] == 'bilateral': cancer.has_laterality.append(self.onto.bilateral)
            elif factors["laterality"] == 'unilateral': cancer.has_laterality.append(self.onto.unilateral)
            elif factors["laterality"] == 'multifocal': cancer.has_laterality.append(self.onto.multifocal)

        if 'cancerTissueOrigin' in factors:
            #  breast
            if factors['cancerTissueOrigin'] == 'ductal': cancer.has_cancer_tissue_origin.append(self.onto.ductal)
            elif factors['cancerTissueOrigin'] == 'lobular': cancer.has_cancer_tissue_origin.append(self.onto.lobular)
            #  colorectal
            if factors['cancerTissueOrigin'] == 'proximal': cancer.has_cancer_tissue_origin.append(self.onto.proximal)
            elif factors['cancerTissueOrigin'] == 'distal': cancer.has_cancer_tissue_origin.append(self.onto.distal)
            #  endometrial, ovarian
            if factors['cancerTissueOrigin'] == 'epithelial': cancer.has_cancer_tissue_origin.append(self.onto.epithelial)
            #  pancreatic                
            if factors['cancerTissueOrigin'] == 'exocrine': cancer.has_cancer_tissue_origin.append(self.onto.exocrine)

            if factors['cancerTissueOrigin'] == 'other': cancer.has_cancer_tissue_origin.append(self.onto.other)

        if 'erStatus' in factors: 
            if factors['erStatus'] == 'positive': cancer.has_hormone_status.append(self.onto.er_positive)
            elif factors['erStatus'] == 'negative': cancer.has_hormone_status.append(self.onto.er_negative)

        if 'prStatus' in factors:
            if factors['prStatus'] == 'positive': cancer.has_hormone_status.append(self.onto.pr_positive)
            elif factors['prStatus'] == 'negative': cancer.has_hormone_status.append(self.onto.pr_negative)

        if 'her2Status' in factors:
            if factors['her2Status'] == 'positive': cancer.has_hormone_status.append(self.onto.her2_positive)
            elif factors['her2Status'] == 'negative': cancer.has_hormone_status.append(self.onto.her2_negative)

        if 'isInvasive' in factors:
            if factors['isInvasive']: cancer.is_invasive = True

        if 'isMetastatic' in factors:
            if factors['isMetastatic']: cancer.is_metastatic = True

        if 'mmrDeficiency' in factors: 
            if factors['mmrDeficiency']: cancer.has_factor.append(self.onto.mmr_deficient)

        if 'gleasonScore' in factors: cancer.has_gleason_score = factors['gleasonScore']

        if 'kidneyLocation' in factors:
            if factors["kidneyLocation"] == 'single': cancer.has_cancer_tissue_origin.append(self.onto.single_spot)
            elif factors["kidneyLocation"] == 'multiple': cancer.has_cancer_tissue_origin.append(self.onto.multiple_spot)

        if 'kidneyHistology' in factors: 
            if factors["kidneyHistology"] == 'clear_cell': cancer.has_histology.append(self.onto.clear_cell)
            elif factors["kidneyHistology"] == 'papillary_type_I': cancer.has_histology.append(self.onto.papillary_type_I)
            elif factors["kidneyHistology"] == 'papillary_type_II': cancer.has_histology.append(self.onto.papillary_type_II)
            elif factors["kidneyHistology"] == 'collecting_duct': cancer.has_histology.append(self.onto.collecting_duct)
            elif factors["kidneyHistology"] == 'tubulopapillary': cancer.has_histology.append(self.onto.tubulopapillary)
            elif factors["kidneyHistology"] == 'bhd_related': cancer.has_histology.append(self.onto.bhd_related)

        return cancer

    def get_cancer_by_id(self, concept_id):
        return self.onto.search_one(id=concept_id)

    def set_all_cancers_disjoint(self):
        if self.onto.Cancer.instances(): AllDifferent([c for c in self.onto.Cancer.instances()])