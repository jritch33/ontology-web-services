
from owlready2 import *

class PolypService:
    def __init__(self, ontology_service): 
        self.ontology_service = ontology_service
        self.onto = self.ontology_service.get_onto()
        self.polyp_dict = self.__polyp_dict_init()
    
    def __polyp_dict_init(self):
        polyp_dict = {}
        polyps = self.onto.search(is_a = self.onto.Polyp, is_core_concept = True)
        for p in polyps:
            polyp_dict[p.id[0]] = {
                'name': p.display[0].replace(' ', '_'),
                'type': p.name
            }
        return polyp_dict

    def get_polyp_dict(self):
        return self.polyp_dict

    def create_polyp(self, condition, condition_count, patient_name):
        polyp_type = self.polyp_dict[condition['conceptId']]['type']
        polyp_name = self.polyp_dict[condition['conceptId']]['name']
        polyp = self.onto[polyp_type](name = '_'.join((patient_name, polyp_name, condition_count)))
        polyp.display.append(polyp_name.replace('_', ' '))
        return polyp

    def add_polyp_to_ontology(self, condition, condition_count, patient_name):
        polyp = self.create_polyp(condition, condition_count, patient_name)
        return polyp

    def set_all_polyps_disjoint(self):
        if self.onto.Polyp.instances(): AllDifferent([p for p in self.onto.Polyp.instances()])