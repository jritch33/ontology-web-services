from owlready2 import *

class GeneService:
    def __init__(self, ontology_service): 
        self.ontology_service = ontology_service
        self.onto = self.ontology_service.get_onto()
        self.gene_dict = self.__gene_dict_init()
       
    def __gene_dict_init(self):
        gene_dict = {}
        genes = self.onto.search(is_a = self.onto.CancerGene, is_core_concept = True)
        for g in genes:
            gene_dict[g.id[0]] = {
                'gene_symbol': g.name
            }
        return gene_dict

    def get_gene_dict(self):
        return self.gene_dict

    def get_genes(self):
        return self.onto.CancerGene.subclasses()

    def create_gene_mutation(self, gene_symbol, patient_name):
        gene_mutation = self.onto[gene_symbol](name = '_'.join((patient_name, gene_symbol)))
        return gene_mutation

    def create_syndromes(self, gene, patient_name):
        syndromes = []
        if gene.mutation_causes_syndrome:
            for syn in gene.mutation_causes_syndrome:
                syndromes.append(self.onto[syn](name = '_'.join((patient_name, gene.name, syn))))
        return syndromes

    def add_gene_to_ontology(self, gene, patient_name):
        gene_symbol = self.gene_dict[gene['conceptId']]['gene_symbol']
        onto_gene = self.create_gene_mutation(gene_symbol, patient_name)
        syndromes = self.create_syndromes(onto_gene, patient_name)
        return {
            'gene': onto_gene,
            'syndromes': syndromes
        }

    def set_all_genes_disjoint(self):
        if self.onto.CancerGene.instances(): AllDifferent([g for g in self.onto.CancerGene.instances()])
    
    def set_all_syndromes_disjoint(self):
        if self.onto.Syndrome.instances(): AllDifferent([s for s in self.onto.Syndrome.instances()])