from owlready2 import *
import json
from oao.oao import Oao
from oao.patient_oao import PatientOao
from oao.cancer_oao import CancerOao
from oao.gene_oao import GeneOao
from tools.rule_engine import RuleEngine

class OntologyService():
    def __init__(self, onto_id):
        print('Loading ontology...')
        self.onto_id = onto_id
        self.world = World()
        self.onto = self.world.get_ontology('repository/' + self.onto_id + '_onto.owl').load()
        self.oao = Oao(self)
        self.patient_oao = PatientOao(self)
        self.cancer_oao = CancerOao(self)
        self.gene_oao = GeneOao(self)
        print('Loaded.')
        acmg_secondary_rules = [ 'ACMGAtRiskFromMotherPatient', 'ACMGAtRiskFromFatherPatient', 'ACMGAtRiskFromSiblingPatient' ]
        nccn_secondary_rules = [ 'CRIT1.01', 'CRIT1.03', 'CRIT1.17', 'GENE1.01', 'GENE1.02', 'POLYP1.02', 'POLYP1.03', 'PJS1.04', 'JPS1.04', 'LS1.01', 'LS1.02' ]
        cancer_secondary_rules = self.__expand_rule_set([ 'UrothelialCancer', 'LiFraumeniCancer' ]) + self.__expand_rule_set([ 'LynchCancer' ])
        if onto_id != 'hereditary_cancer_core':
            cancer = { 
                'class': 'Cancer',
                'rules': [ self.__get_rule_set('Cancer') ] + cancer_secondary_rules
            }
            syndrome = { 
                'class': 'Syndrome',
                'rules': [ self.__get_rule_set('Syndrome') ] 
            }
            patient = { 
                'class': 'Patient',
                'rules': self.__get_patient_rule_set() 
            }
            if onto_id == 'hereditary_cancer_acmg':
                patient['rules'].append(self.__get_acmg_patient_rule_set()) 
                patient['rules'].append(acmg_secondary_rules)
            elif onto_id == 'hereditary_cancer_nccn':
                patient['rules'].append(self.__get_nccn_patient_rule_set()) 
                patient['rules'].append(nccn_secondary_rules)
            else:
                patient['rules'].append(self.__get_acmg_patient_rule_set() + self.__get_nccn_patient_rule_set())
                patient['rules'].append(acmg_secondary_rules + nccn_secondary_rules)

            self.rule_sets = [
                cancer,
                syndrome,
                patient
            ]
            
            self.engine = RuleEngine(self.rule_sets, self.patient_oao, 'hereditary_cancer', self.onto)

    def __expand_rule_set(self, classes):
        r = []
        for klass in classes:
            r += self.__get_rule_set(klass)
        return r

    def __get_rule_set(self, klass): 
        rule_set = [ klass ]
        for x in self.onto[klass].descendants(False, world=self.world):
            rule_set.append(x.name)
        return rule_set

    def __get_patient_rule_set(self):
        rule_set = [
            self.__get_rule_set('CancerGeneMutationPatient'),
            self.__get_rule_set('CancerPatient'),
            self.__get_rule_set('PolypPatient'),
            ['AshkenaziAncestryPatient', 'FemalePatient','MalePatient']
        ]
        return rule_set

    def __get_acmg_patient_rule_set(self):
        rule_set = []
        for c in self.onto['ACMGPatient'].descendants(False, world=self.world):
            rule_set.append(c.name)
        return rule_set

    def __get_nccn_patient_rule_set(self):
        rule_set = []
        for c in self.onto['NCCNPatient'].descendants(False, world=self.world):
            rule_set.append(c.name)
        return rule_set

    def __test(self, onto):
        for c in Thing.subclasses(False, world=onto.world):
            self.rule_sets.append({
                'class': c.name,
                'rules': self.__get_rule_set(c.name)
            })

    def get_onto(self):
        return self.onto

    def get_world(self):
        return self.world

    def get_criteria(self, klass):
        return self.onto[klass].criteria[0]
    
    def get_concept_by_id(self, concept_id):
        concept = self.oao.get_concept_by_id(concept_id)
        return concept.name if concept else None

    def sync(self):
        with self.onto:
            print('starting reasoner...')
            sync_reasoner(self.world, debug=0)  
            print('synced')
            
    def execute_rule_engine(self):
        with self.onto:
            print('executing rules...')
            self.engine.execute(self)
            print('executed.')

    def save(self): 
        debug_path = '/../../ontology_definition/repo_debug'
        if os.path.dirname(os.path.abspath(__file__)) + debug_path not in onto_path: 
            onto_path.append(os.path.dirname(os.path.abspath(__file__)) + debug_path)
        with self.onto:
            self.onto.save()
            print('saved')
