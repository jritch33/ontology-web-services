{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "definitions": {
        "patient": {
            "title": "Patient",
            "description": "Family health history information for patient",
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "relationToProband": {
                    "type": "string",
                    "enum": [
                        "self",
                        "MTH",
                        "FTH",
                        "BRO",
                        "SIS",
                        "SON",
                        "DAU",
                        "MAUNT",
                        "PAUNT",
                        "MUNCLE",
                        "PUNCLE",
                        "NIECE",
                        "NEPHEW",
                        "MGRMTH",
                        "PGRMTH",
                        "MGRFTH",
                        "PGRFTH",
                        "GSON",
                        "GDAUC"
                    ]
                },
                "age": {
                    "$ref": "#/definitions/age"
                },
                "sex": {
                    "type": "string",
                    "enum": [
                        "male",
                        "female"
                    ]
                },
                "conditions": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/condition"
                    }
                },
                "relatives": {
                    "description": "array of patient objects that represent family members",
                    "type": "array",
                    "items": {
                        "type": "object"
                    }
                },
                "traits": {
                    "$ref": "#/definitions/traits"
                },
                "colonoscopy": {
                    "$ref": "#/definitions/colonoscopy"
                },
                "geneMutations": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/geneMutation"
                    }
                },
                "mammogram": {
                    "$ref": "#/definitions/mammogram"
                },
                "smokingHistory": {
                    "$ref": "#/definitions/smokingHistory"
                },
                "drinkingHistory": {
                    "$ref": "#/definitions/drinkingHistory"
                },
                "ageMenstruation": {
                    "$ref": "#/definitions/age"
                },
                "ageMenopause": {
                    "$ref": "#/definitions/age"
                }
            },
            "required": [
                "relationToProband"
            ]
        },
        "age": {
            "description": "A patient's age",
            "type": "integer",
            "minimum": 0
        },
        "condition": {
            "description": "Patient health diagnosis status i.e. breast cancer and related info",
            "type": "object",
            "properties": {
                "conceptId": {
                    "type": "string"
                },
                "ageOnset": {
                    "$ref": "#/definitions/age"
                },
                "factors": {
                    "anyOf": [
                        {
                            "$ref": "#/definitions/breastCancerFactors"
                        },
                        {
                            "$ref": "#/definitions/kidneyCancerFactors"
                        },
                        {
                            "$ref": "#/definitions/ovarianCancerFactors"
                        },
                        {
                            "$ref": "#/definitions/prostateCancerFactors"
                        },
                        {
                            "$ref": "#/definitions/endometrialCancerFactors"
                        },
                        {
                            "$ref": "#/definitions/colorectalCancerFactors"
                        }
                    ]
                }
            },
            "required": [
                "conceptId"
            ]
        },
        "breastCancerFactors": {
            "description": "Disease factors that describe breast cancer",
            "type": "object",
            "properties": {
                "laterality": {
                    "$ref": "#/definitions/laterality"
                },
                "cancerTissueOrigin": {
                    "type": "string",
                    "enum": [
                        "Ductal",
                        "Lobular",
                        "other"
                    ]
                },
                "erStatus": {
                    "type": "string",
                    "enum": [
                        "ER positive",
                        "ER negative"
                    ]
                },
                "prStatus": {
                    "type": "string",
                    "enum": [
                        "PR positive",
                        "PR negative"
                    ]
                },
                "her2Status": {
                    "type": "string",
                    "enum": [
                        "HER2 positive",
                        "HER2 negative"
                    ]
                }
            }
        },
        "laterality": {
            "title": "Laterality",
            "description": "Laterality of a cancer",
            "type": "string",
            "enum": [
                "Bilateral",
                "Unilateral"
            ]
        },
        "kidneyCancerFactors": {
            "description": "Disease factors that describe kidney cancer",
            "type": "object",
            "properties": {
                "laterality": {
                    "$ref": "#/definitions/laterality"
                },
                "kidneyHistology": {
                    "type": "string",
                    "enum": [
                        "clear-cell",
                        "papillary-type-1",
                        "papillary-type-2",
                        "collecting-duct",
                        "tubulopapillary",
                        "bhd-related"
                    ]
                },
                "kidneyLocation": {
                    "type": "string",
                    "enum": [
                        "Single spot",
                        "Multiple spot"
                    ]
                }
            }
        },
        "ovarianCancerFactors": {
            "description": "Disease factors that describe ovarian cancer",
            "type": "object",
            "properties": {
                "isInvasive": {
                    "type": "boolean"
                }
            }
        },
        "prostateCancerFactors": {
            "description": "Disease factors that describe prostate cancer",
            "type": "object",
            "properties": {
                "gleasonScore": {
                    "type": "integer",
                    "maximum": 10,
                    "minimum": 2
                },
                "isMetastatic": {
                    "type": "boolean"
                }
            }
        },
        "endometrialCancerFactors": {
            "description": "Disease factors that describe endometrial cancer",
            "type": "object",
            "properties": {
                "cancerTissueOrigin": {
                    "type": "string",
                    "enum": [
                        "Epithelial"
                    ]
                },
                "isSynchronous": {
                    "type": "boolean"
                },
                "isMetachronous": {
                    "type": "boolean"
                },
                "mmrDeficiency": {
                    "type": "boolean"
                }
            }
        },
        "colorectalCancerFactors": {
            "description": "Disease factors that describe colorectal cancer",
            "type": "object",
            "properties": {
                "cancerTissueOrigin": {
                    "type": "string",
                    "enum": [
                        "Proximal",
                        "Distal"
                    ]
                },
                "isSynchronous": {
                    "type": "boolean"
                },
                "isMetachronous": {
                    "type": "boolean"
                },
                "mmrDeficiency": {
                    "type": "boolean"
                }
            }
        },
        "traits": {
            "description": "Family health history information for patient",
            "type": "object",
            "properties": {
                "ancestry": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "skinColor": {
                    "type": "string",
                    "enum": [
                        "white",
                        "olive",
                        "brown",
                        "black"
                    ]
                },
                "bodyWeight": {
                    "description": "weight in kg",
                    "type": "integer",
                    "minimum": 0
                },
                "bodyHeight": {
                    "description": "height in cm",
                    "type": "integer",
                    "minimum": 0
                }
            }
        },
        "colonoscopy": {
            "description": "Colonoscopy findings for a patient",
            "type": "object",
            "properties": {
                "ageLastColonoscopy": {
                    "$ref": "#/definitions/age"
                },
                "polyps": {
                    "type": "object",
                    "properties": {
                        "adenomatous": {
                            "type": "object",
                            "properties": {
                                "tubular": {
                                    "type": "integer"
                                },
                                "villous": {
                                    "type": "integer"
                                },
                                "tubulovillous": {
                                    "type": "integer"
                                },
                                "serrated": {
                                    "type": "object",
                                    "properties": {
                                        "sessile": {
                                            "type": "object",
                                            "properties": {
                                                "proximal": {
                                                    "type": "integer"
                                                },
                                                "distal": {
                                                    "type": "integer"
                                                },
                                                "throughout": {
                                                    "type": "integer"
                                                }
                                            }
                                        },
                                        "traditional": {
                                            "type": "integer"
                                        }
                                    }
                                }
                            }
                        },
                        "hamartomatous": {
                            "type": "object",
                            "properties": {
                                "juvenile": {
                                    "type": "integer"
                                },
                                "peutzJeghers": {
                                    "type": "integer"
                                }
                            }
                        }
                    }
                }
            },
            "required": [
                "polyps"
            ]
        },
        "geneMutation": {
            "description": "Cancer Genetic mutation information",
            "type": "object",
            "properties": {
                "gene": {
                    "type": "string"
                },
                "mutationType": {
                    "type": "string",
                    "enum": [
                        "missense",
                        "splice site",
                        "frameshift",
                        "in frame",
                        "deletion",
                        "nonsense",
                        "other"
                    ]
                },
                "hgvs": {
                    "type": "string"
                }
            }
        },
        "mammogram": {
            "description": "Mammogram information for patient",
            "type": "object",
            "properties": {
                "hadMammogram": {
                    "type": "boolean"
                },
                "needsMammogram": {
                    "type": "boolean"
                },
                "monthsSinceLastMammogram": {
                    "type": "integer",
                    "minimum": 0
                }
            }
        },
        "smokingHistory": {
            "description": "Smoking history for patient",
            "type": "object",
            "properties": {
                "hasSmoked": {
                    "type": "boolean"
                },
                "currentSmoker": {
                    "type": "boolean"
                },
                "ageStarted": {
                    "$ref": "#/definitions/age"
                },
                "ageQuit": {
                    "$ref": "#/definitions/age"
                },
                "packsPerDay": {
                    "type": "integer",
                    "minimum": 0
                },
                "smokingFrequency": {
                    "type": "string",
                    "enum": [
                        "daily",
                        "some-days",
                        "never"
                    ]
                },
                "packYearsSmoked": {
                    "type": "number",
                    "minimum": 0
                }
            }
        },
        "drinkingHistory": {
            "description": "Drinking history for patient",
            "type": "object",
            "properties": {
                "drinkingFrequency": {
                    "type": "string",
                    "enum": [
                        "never",
                        "once-monthly",
                        "2-3-weekly",
                        "2-4-monthly",
                        "4-or-more-weekly"
                    ]
                }
            }
        },
        "ageMenstruation": {
            "$ref": "#/definitions/age"
        },
        "ageMenopause": {
            "$ref": "#/definitions/age"
        }
    },
    "patient":{
        "$ref": "#/definitions/patient"
    }
    
}