import os
import json
import jsonschema
import jsonref
from flask import Flask, jsonify, make_response, request
from flask_restplus import Api, Resource, fields, abort
from flask_swagger_ui import get_swaggerui_blueprint
from flask_expects_json import expects_json
from flask_cors import CORS
from owlready2 import *
from services.patient_service import PatientService
from services.ontology_service import OntologyService
from tools.schema.schema_validator import JSONValidator
from pprint import pprint

hc_json_validator = JSONValidator('hereditary_cancer_schema', 'hereditary_cancer')

with open('static/request-definitions.json') as rd:
    p = jsonref.load(rd)
    patient_schema = p['patient']

api = Flask(__name__)
CORS(api)

swagger_url = '/swagger'
api_url = '/static/swagger.json'
swaggerui_blueprint = get_swaggerui_blueprint(
    swagger_url,
    api_url,
    config={
        'app_name': "FHx Classification API"
    }
)

api.register_blueprint(swaggerui_blueprint, url_prefix=swagger_url)

@api.route('/recommendations', methods=['POST'])
@expects_json(patient_schema)
def assess_fhx():
    if not request.get_json():
        abort(400)
    data = request.get_json(force=True)

    ontology_service = OntologyService('hereditary_cancer')
    hereditary_cancer_patient_service = PatientService(ontology_service)
    hereditary_cancer_asssessment = hereditary_cancer_patient_service.assess_patient_risk(data)
    print('Risk assessment completed!')

    hc_json_validator.validate_schema(hereditary_cancer_asssessment)

    return hereditary_cancer_asssessment['patientRecommendations'], 200

@api.route('/recommendations/acmg', methods=['POST'])
@expects_json(patient_schema)
def assess_fhx_acmg():
    if not request.get_json():
        abort(400)
    data = request.get_json(force=True)

    ontology_service = OntologyService('hereditary_cancer_acmg')
    hereditary_cancer_patient_service = PatientService(ontology_service)
    hereditary_cancer_asssessment = hereditary_cancer_patient_service.assess_patient_risk(data)
    print('ACMG risk assessment completed!')

    hc_json_validator.validate_schema(hereditary_cancer_asssessment)

    return hereditary_cancer_asssessment['patientRecommendations'], 200

@api.route('/recommendations/nccn', methods=['POST'])
@expects_json(patient_schema)
def assess_fhx_nccn():
    if not request.get_json():
        abort(400)
    data = request.get_json(force=True)

    ontology_service = OntologyService('hereditary_cancer_nccn')
    hereditary_cancer_patient_service = PatientService(ontology_service)
    hereditary_cancer_asssessment = hereditary_cancer_patient_service.assess_patient_risk(data)
    print('NCCN risk assessment completed!')

    hc_json_validator.validate_schema(hereditary_cancer_asssessment)

    return hereditary_cancer_asssessment['patientRecommendations'], 200

@api.route('/cancer_risk/assessment', methods=['POST'])
@expects_json(patient_schema)
def assessment():
    if not request.get_json():
        abort(400)
    data = request.get_json(force=True)

    ontology_service = OntologyService('hereditary_cancer')
    hereditary_cancer_patient_service = PatientService(ontology_service)
    hereditary_cancer_asssessment = hereditary_cancer_patient_service.assess_patient_risk(data)
    print('Risk assessment completed!')

    hc_json_validator.validate_schema(hereditary_cancer_asssessment)

    return hereditary_cancer_asssessment, 200

@api.route('/cancer_risk/assessment/acmg', methods=['POST'])
@expects_json(patient_schema)
def assessment_acmg():
    if not request.get_json():
        abort(400)
    data = request.get_json(force=True)

    ontology_service = OntologyService('hereditary_cancer_acmg')
    hereditary_cancer_patient_service = PatientService(ontology_service)
    hereditary_cancer_asssessment = hereditary_cancer_patient_service.assess_patient_risk(data)
    print('ACMG risk assessment completed!')

    hc_json_validator.validate_schema(hereditary_cancer_asssessment)

    return hereditary_cancer_asssessment, 200

@api.route('/cancer_risk/assessment/nccn', methods=['POST'])
@expects_json(patient_schema)
def assessment_nccn():
    if not request.get_json():
        abort(400)
    data = request.get_json(force=True)

    ontology_service = OntologyService('hereditary_cancer_nccn')
    hereditary_cancer_patient_service = PatientService(ontology_service)
    hereditary_cancer_asssessment = hereditary_cancer_patient_service.assess_patient_risk(data)
    print('NCCN risk assessment completed!')

    hc_json_validator.validate_schema(hereditary_cancer_asssessment)

    return hereditary_cancer_asssessment, 200

@api.route('/cancer_risk/<string:criteria>')
def get_criteria(criteria):
    ontology_service = OntologyService('hereditary_cancer')
    hereditary_cancer_patient_service = PatientService(ontology_service)

    print("retrieving criteria text...")
    crit = ontology_service.get_criteria(criteria)
    print("criteria text retrieved: " + str(crit))
    return crit

@api.route('/cancer_risk/concept_id/<string:concept_id>')
def get_concept(concept_id):
    ontology_service = OntologyService('hereditary_cancer')
    hereditary_cancer_patient_service = PatientService(ontology_service)

    print("retrieving concept..." + concept_id)
    concept = ontology_service.get_concept_by_id(concept_id)
    print("concept retrieved: " + str(concept))
    return concept


if __name__ == '__main__':
    api.run()
