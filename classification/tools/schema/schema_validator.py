import os
import jsonschema
import jsonref
from pprint import pprint
import json

class JSONValidator:
    def __init__(self, schema_dir, schema_file):
        self.schema_dir = schema_dir
        self.schema_file = schema_file

    def load_schema(self):
        schema = os.path.dirname(os.path.abspath(__file__)) + '/' + self.schema_dir + '/' + self.schema_file + '.json'
        base_uri = 'file://{}/'.format(os.path.dirname(schema))
        with open(schema) as schema_file:
            return jsonref.loads(schema_file.read(), base_uri=base_uri, jsonschema=True)

    def validate_schema(self, obj):
        schema = self.load_schema()
        try:
            jsonschema.validate(obj, schema)
        except jsonschema.ValidationError as e:
            print({ 'message': 'ValidationError, invalid json: ' + str(e) })

    def get_schema(self):
        schema = self.load_schema()
        return schema