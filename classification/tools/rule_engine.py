import os
import json
from owlready2 import Thing
import inspect

class RuleEngine:
    def __init__(self, rule_sets, patient_oao, def_file, onto):
        self.def_file = def_file
        self.df = self.__load_df()
        self.rule_sets = rule_sets
        self.onto = onto
        self.patient_oao = patient_oao

    ##  FUTURE WORK - order equivalencies for execution by ordering them in a list 
    ##  after their dependencies.

    def __load_df(self):
        with open(os.path.dirname(os.path.abspath(__file__)) + '/../../ontology_definition/' + self.def_file + "_definition_file.json") as df_file:
            df = json.load(df_file)
            return df

    def eval_class(self, klass, eq_data, obj):
        return obj.has_super_class(self.onto[eq_data['class']])

    def eval_value(self, klass, eq_data, obj):        
        value = eq_data['value'] if isinstance(eq_data['value'], bool) else self.onto[eq_data['value']]
        return obj.has_value(self.onto[eq_data['property']], value)

    def some(self, klass, eq_data, obj):
        return obj.has_some(self.onto[eq_data['property']], self.onto[eq_data['class']])
    
    def and_some(self, klass, eq_data, obj):
        classes = [ self.onto[k['class']] for k in eq_data['and'] ]
        passed = [ obj.has_some(self.onto[eq_data['property']], k) for k in classes ]
        return all(passed)

    def or_some(self, klass, eq_data, obj):
        classes = [ self.onto[k['class']] for k in eq_data['or'] ]
        passed = [ obj.has_some(self.onto[eq_data['property']], k) for k in classes ]
        return any(passed)

    def eval_some(self, klass, eq_data, obj):
        if 'and' in eq_data:
            return self.and_some(klass, eq_data, obj)
        elif 'or' in eq_data:
            return self.or_some(klass, eq_data, obj)
        return self.some(klass, eq_data, obj)
    
    def eval_comparison(self, klass, eq_data, obj):
        data_prop_val = obj.get_data_prop_value(self.onto[eq_data['property']])
        if data_prop_val is False: 
            return False
        if eq_data['restriction'] == 'lt': 
            return data_prop_val < eq_data['value']
        elif eq_data['restriction'] == 'lte': 
            return data_prop_val <= eq_data['value']
        elif eq_data['restriction'] == 'gt': 
            return data_prop_val > eq_data['value']
        elif eq_data['restriction'] == 'gte':
            return data_prop_val >= eq_data['value']
        else: print("Restriction must be lt, lte, gt, or gte")

    def eval_card(self, klass, count, card, restriction):
        if restriction == 'min': return count >= card
        elif restriction == 'max': return count <= card
        elif restriction == 'exactly': return count == card

    def cardinality(self, klass, eq_data, obj):
        card = eq_data['cardinality']
        restriction = eq_data['restriction']
        count = obj.get_cardinality_value_count(self.onto[eq_data['property']], [ self.onto[eq_data['class']] ])
        return self.eval_card(klass, count, card, restriction)

    def or_cardinality(self, klass, eq_data, obj):
        card = eq_data['cardinality']
        restriction = eq_data['restriction']
        classes = [ self.onto[klass['class']] for klass in eq_data['or'] ]
        count = obj.get_cardinality_value_count(self.onto[eq_data['property']], classes)
        return self.eval_card(klass, count, card, restriction)

    def eval_cardinality(self, klass, eq_data, obj):
        restriction = eq_data['restriction']
        if 'or' in eq_data and restriction in ['max', 'min']:
            return self.or_cardinality(klass, eq_data, obj)
        elif 'class' in eq_data and restriction in ['max', 'min']:
            return self.cardinality(klass, eq_data, obj)

    def eval_restriction_type(self, klass, eq_data, obj):
        if eq_data['restriction'] == 'some':
            return self.eval_some(klass, eq_data, obj)
        elif eq_data['restriction'] in ['lt', 'lte', 'gte', 'gt']:
            return self.eval_comparison(klass, eq_data, obj)
        elif eq_data['restriction'] in ['min', 'max']:
            return self.eval_cardinality(klass, eq_data, obj)

    def eval_prop_clause(self, klass, eq_data, obj):
        if eq_data['restriction'] == 'value':
            return self.eval_value(klass, eq_data, obj)
        else:
            return self.eval_restriction_type(klass, eq_data, obj)

    def eval_not(self, klass, eq_data, obj):
        passed = []
        for clause in eq_data:
            if 'property' in clause:
                passed.append(not self.eval_prop_clause(klass, clause, obj))
            else:
                passed.append(not self.eval_class(klass, clause, obj))
        
        return all(passed)

    def eval_or(self, klass, eq_data, obj):
        passed = []
        for clause in eq_data:
            if 'property' in clause:
                passed.append(self.eval_prop_clause(klass, clause, obj))
            elif 'and' in clause:
                passed.append(self.eval_and(klass, clause['and'], obj))
            elif 'not' in clause:
                passed.append(self.eval_not(klass, clause['not'], obj))
            else:
                passed.append(self.eval_class(klass, clause, obj))
        return any(passed)

    def eval_and(self, klass, eq_data, obj):
        passed = []
        for clause in eq_data:
            if 'property' in clause:
                passed.append(self.eval_prop_clause(klass, clause, obj))
            elif 'and' in clause and not 'property' in clause:
                passed.append(self.eval_and(klass, clause['and'], obj))
            elif 'or' in clause and not 'property' in clause:
                passed.append(self.eval_or(klass, clause['or'], obj))
            elif 'not' in clause:
                passed.append(self.eval_not(klass, clause['not'], obj))
            else: 
                passed.append(self.eval_class(klass, clause, obj))
        return all(passed)

    def evaluate_rule(self, klass, eq_data, obj):
        if 'property' in eq_data:
            return self.eval_prop_clause(klass, eq_data, obj)
        elif 'and' in eq_data:
            return self.eval_and(klass, eq_data['and'], obj)
        elif 'or' in eq_data:
            return self.eval_or(klass, eq_data['or'], obj)
        elif 'not' in eq_data:
            return self.eval_not(klass, eq_data['not'], obj)
        else:
            return self.eval_class(klass, eq_data, obj)

    def walk_tree(self, classes, obj, rules):
        for klass in classes:
            if klass in rules and 'equivalencies' in classes[klass]:
                if self.evaluate_rule(klass, classes[klass]['equivalencies'], obj):
                    obj.add_is_a(self.onto[klass])
            if 'subclasses' in classes[klass] and classes[klass]['subclasses']:
                self.walk_tree(classes[klass]['subclasses'], obj, rules) 

    def execute(self, onto_service):
        with self.onto:
            for klass in self.rule_sets:
                for rs in klass['rules']:
                    for i in self.onto[klass['class']].instances():
                        self.walk_tree(self.df['classes'], i, rs)