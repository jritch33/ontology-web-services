from owlready2 import *
from pprint import pprint

class GeneOao:
    def __init__(self, ontology_service):
        self.ontology_service = ontology_service
        self.onto = self.ontology_service.get_onto()
        self.build_gene_class_def()

    def build_gene_class_def(self):
        with self.onto:
            class CancerGene(Thing):
                pass