from owlready2 import *

class PatientOao:
    def __init__(self, ontology_service): 
        self.ontology_service = ontology_service
        self.onto = self.ontology_service.get_onto()
        self.build_patient_class_def()

    def build_patient_class_def(self):
        with self.onto:
            class Patient(Thing): 
                def add_patient_name(self, data):
                    if 'fullName' in data and 'lastName' in data:
                        self.patient_name.append(data['fullName'] + ' ' + data['lastName'])
                        self.patient_first_name.append(data['fullName'])
                    elif 'fullName' in data:
                        self.patient_name.append(data['fullName'])
                        self.patient_first_name.append(data['fullName'])
                    else:
                        self.patient_name.append('')
                        self.patient_first_name.append('')

                def add_age_sex(self, data, onto):
                    if 'age' in data:
                        self.has_age = data['age']
                    if 'sex' in data:
                        if data['sex'] == 'male': self.has_sex.append(onto.male)
                        elif data['sex'] == 'female': self.has_sex.append(onto.female)
                        else: self.has_sex.append(onto.unspecified)
                    else: self.has_sex.append(onto.unspecified)
            
                def add_deceased(self, data, onto):
                    if 'deceased' in data:
                        if data['deceased'] is not None: self.is_deceased = data['deceased']
                    if 'ageOfDeath' in data:
                        if data['ageOfDeath'] is not None: self.has_age_of_death = data['ageOfDeath']
                    if 'yearOfDeath' in data:
                        if data['yearOfDeath'] is not None: self.has_year_of_death = data['yearOfDeath']

                def add_conditions(self, data, cancer_service, polyp_service):
                    cancer_count_dict = {}
                    polyp_count_dict = {}
                    cancers = []
                    polyps = []
                    for c in data['conditions']:
                        if c['conceptId'] in cancer_service.get_cancer_dict():
                            if c['conceptId'] in cancer_count_dict: cancer_count_dict[c['conceptId']] += 1 
                            else: cancer_count_dict[c['conceptId']] = 1
                            condition_count = str(cancer_count_dict[c['conceptId']])
                            if 'factors' in c: factors = c['factors']
                            else: factors = []
                            cancers.append(cancer_service.add_cancer_to_ontology(c, condition_count, factors, self.name))
                        elif c['conceptId'] in polyp_service.get_polyp_dict():
                            if c['conceptId'] in polyp_count_dict: polyp_count_dict[c['conceptId']] += 1 
                            else: polyp_count_dict[c['conceptId']] = 1
                            condition_count = str(polyp_count_dict[c['conceptId']])
                            polyps.append(polyp_service.add_polyp_to_ontology(c, condition_count, self.name))
                    
                    self.has_cancer = cancers
                    self.has_polyp = polyps
                                            
                    cancer_service.set_all_cancers_disjoint()
                    polyp_service.set_all_polyps_disjoint()
                    
                def add_ancestry(self, ancestry):
                    self.has_ancestry.append(ancestry)

                def add_skin_color(self, skin_color):
                    self.has_trait.append(skin_color)
                
                def add_biometrics(self, biometrics):
                    if 'bodyWeight' in biometrics: self.has_weight = biometrics['bodyWeight'] 
                    if 'bodyHeight' in biometrics: self.has_height = biometrics['bodyHeight']

                def set_relative_relations(self, relative, relation, onto, is_fdr):
                    if is_fdr:
                        self.has_fdr.append(relative)
                        relative.has_fdr.append(self)  
                    if relation == 'MTH' or relation[0:1] == 'M': 
                        self.has_maternal_close_relative.append(relative)
                        relative.has_maternal_close_relative.append(self)
                        relative.has_paternal_close_relative.append(self)
                    elif relation == 'FTH' or relation[0:1] == 'P': 
                        self.has_paternal_close_relative.append(relative)
                        relative.has_maternal_close_relative.append(self)
                        relative.has_paternal_close_relative.append(self)
                        
                    else: #  is sibling or descendant
                        self.has_maternal_close_relative.append(relative)
                        self.has_paternal_close_relative.append(relative)
                        if relation == 'SIS' or relation == 'BRO':
                            relative.has_maternal_close_relative.append(self)
                            relative.has_paternal_close_relative.append(self)
                        if relation == 'SON' or relation == 'DAU':
                            if onto.female in self.has_sex:
                                relative.has_maternal_close_relative.append(self)
                            elif onto.male in self.has_sex:
                                relative.has_paternal_close_relative.append(self)
                        #  If relation to proband is niece, nephew, or grandchild we can't 
                        #  infer the paternal or maternal relationship of the proband to the 
                        #  niece, nephew, or grandchild without additional information.

                def set_parent_child_relations(self, relative, relation, onto):
                    if relation == 'MTH':
                        self.has_mother.append(relative)
                    elif relation == 'FTH':
                        self.has_father.append(relative)
                    relative.has_child.append(self)

                def set_sibling_relations(self, relative):
                    self.has_sibling.append(relative)
                    relative.has_sibling.append(self)

                def relate(self, relative, relation, onto, is_fdr):
                    self.set_relative_relations(relative, relation, onto, is_fdr)
                    if relation in ['MTH', 'FTH']: 
                        self.set_parent_child_relations(relative, relation, onto)
                    if relation in ['BRO', 'SIS']:
                        self.set_sibling_relations(relative)
                        
                def count_polyps(self, data):
                    ham_count = 0
                    pj_count = 0
                    juv_count = 0
                    aden_count = 0
                    tub_count = 0
                    vil_count = 0
                    tub_vil_count = 0
                    ser_count = 0
                    ses_count = 0
                    proximal_count = 0
                    distal_count = 0
                    tout_count = 0
                    trad_count = 0

                    ham = 'hamartomatous'
                    pj = 'peutzJeghers'
                    juv = 'juvenile'
                    aden = 'adenomatous'
                    tub = 'tubular'
                    vil = 'villous'
                    tub_vil = 'tubulovillous'
                    ser = 'serrated'
                    ses = 'sessile'
                    proximal = 'proximal'
                    distal = 'distal'
                    tout = 'throughout'
                    trad = 'traditional'
                    if ham in data:
                        if pj in data[ham]: pj_count = data[ham][pj]
                        if juv in data[ham]: juv_count = data[ham][juv]
                        ham_count = pj_count + juv_count
                    if aden in data: 
                        if tub in data[aden]: tub_count = data[aden][tub]
                        if vil in data[aden]: vil_count = data[aden][vil]
                        if tub_vil in data[aden]: tub_vil_count = data[aden][tub_vil]
                        if ser in data[aden]:
                            if ses in data[aden][ser]: 
                                if proximal in data[aden][ser][ses]: proximal_count = data[aden][ser][ses][proximal]
                                if distal in data[aden][ser][ses]: distal_count = data[aden][ser][ses][distal]
                                if tout in data[aden][ser][ses]: tout_count = data[aden][ser][ses][tout]
                                ses_count = proximal_count + distal_count + tout_count
                            if trad in data[aden][ser]: trad_count = data[aden][ser][trad]
                            ser_count = ses_count + trad_count
                        aden_count = tub_count + vil_count + tub_vil_count + ser_count
                    return {
                        'pj': pj_count,
                        'juv': juv_count,
                        'ham': ham_count,
                        'aden': aden_count,
                        'tub': tub_count,
                        'vil': vil_count,
                        'tub_vil': tub_vil_count,
                        'ser': ser_count,
                        'ses': ses_count,
                        'proximal': proximal_count,
                        'distal': distal_count,
                        'tout': tout_count,
                        'trad': trad_count
                    }
                    
                def add_colonoscopy(self, data):
                    if 'colonoscopy' in data: 
                        colon = data['colonoscopy']
                        self.has_age_last_colonoscopy = colon['ageLastColonoscopy']
                        if 'polyps' not in colon: return
                        polyps = self.count_polyps(colon['polyps'])
                        self.has_pj_polyp_count = polyps['pj']
                        self.has_juvenile_polyp_count = polyps['juv']
                        self.has_hamartomatous_polyp_count = polyps['ham']
                        self.has_adenomatous_polyp_count = polyps['aden']
                        self.has_serrated_polyp_count = polyps['ser']  
                    else:
                        self.has_age_last_colonoscopy = 0
                        self.has_pj_polyp_count = 0
                        self.has_juvenile_polyp_count = 0
                        self.has_hamartomatous_polyp_count = 0
                        self.has_adenomatous_polyp_count = 0

                def add_gene_mutations(self, data, gene_service):
                    if 'geneMutations' in data and len(data['geneMutations']) != 0: 
                        for m in data['geneMutations']:
                            if m['conceptId'] in gene_service.get_gene_dict():
                                gene_and_syns = gene_service.add_gene_to_ontology(m, self.name)
                                self.has_mutation_in.append(gene_and_syns['gene'])
                                for s in gene_and_syns['syndromes']: self.has_syndrome.append(s)
                    
                    gene_service.set_all_genes_disjoint() 
                    gene_service.set_all_syndromes_disjoint()

                def add_mammogram(self, data):
                    if 'mammogram' in data:
                        if 'monthsSinceLastMammogram' in data['mammogram']:
                            self.has_months_since_last_mammogram = data['mammogram']['monthsSinceLastMammogram']

                def add_smoking_history(self, data):
                    if 'smokingHistory' in data and 'hasSmoked' in data['smokingHistory'] and data['smokingHistory']['hasSmoked']: 
                        age_started_smoking = data['smokingHistory']['ageStarted']
                        if 'ageQuit' in data['smokingHistory']: 
                            age_quit_smoking = data['smokingHistory']['ageQuit']
                            years_smoked = data['age'] - age_started_smoking if data['smokingHistory']['ageQuit'] == 0 else age_quit_smoking - age_started_smoking
                            self.has_years_since_quit_smoking = data['age'] - age_quit_smoking
                        else:
                            years_smoked = data['age'] - age_started_smoking
                            self.has_years_since_quit_smoking = 0
                        
                        packs_per_day = data['smokingHistory']['packsPerDay']
                        self.smokes_packs_per_day = packs_per_day

                        if packs_per_day > 0:
                            #  2 packs a day at 15 years = 30 pack years
                            self.has_pack_years_smoked = years_smoked * packs_per_day if years_smoked * packs_per_day >= 0 else 0
                        else:
                            #  1 pack a day at 30 years = 30 pack years
                            self.has_pack_years_smoked = 0

                        if 'smokingFrequency' in data['smokingHistory']:
                            self.smoking_frequency.append(data['smokingHistory']['smokingFrequency'])
                        else:
                            self.smoking_frequency.append('never')
                    else:
                        self.has_years_since_quit_smoking = 0
                        self.smokes_packs_per_day = 0
                        self.has_pack_years_smoked = 0
                        self.smoking_frequency.append('never')

                def add_drinking_history(self, data):
                    if'patientAlcoholHistory' in data:
                        self.drinking_frequency.append(data['patientAlcoholHistory']['drinkingFrequency'])

                def add_menstruation_history(self, data):
                    if 'ageMenstruation' in data: 
                        self.has_age_menstruation = data['ageMenstruation']
                    else:
                        self.has_age_menstruation = 0

                def add_menopause_history (self, data):
                    if 'ageMenopause' in data: 
                        self.has_age_menopause = data['ageMenopause']
                    else:
                        self.has_age_menopause = 0

                def get_patient_name(self): 
                    return self.patient_name[0]
                
                def get_patient_first_name(self):
                    return self.patient_first_name[0]

                def get_patient_age(self):
                    return self.has_age

                def get_patient_age_menstruation(self):
                    return self.has_age_menstruation

                def get_patient_age_menopause(self):
                    return self.has_age_menopause

                def get_months_since_last_mammogram(self):
                    return self.has_months_since_last_mammogram

                def get_patient_age_last_colonoscopy(self):
                    return self.has_age_last_colonoscopy
                
                def get_pj_polyp_count(self):
                    return self.has_pj_polyp_count

                def get_juvenile_polyp_count(self):
                    return self.has_juvenile_polyp_count

                def get_hamartomatous_polyp_count(self):
                    return self.has_hamartomatous_polyp_count

                def get_adenomatous_polyp_count(self):
                    return self.has_adenomatous_polyp_count

                def get_patient_age_of_death(self):
                    return self.has_age_of_death
                
                def get_patient_year_of_death(self):
                    return self.has_year_of_death

                def get_years_since_quite_smoking(self):
                    return self.has_years_since_quit_smoking

                def get_packs_smoked_per_day(self):
                    return self.smokes_packs_per_day

                def get_sex(self):
                    return self.has_sex[0].name

                def get_ancestry(self):
                    return self.has_ancestry

                def get_relatives(self):
                    m_rel = [ relative for relative in self.has_maternal_close_relative ]
                    p_rel = [ relative for relative in self.has_paternal_close_relative ]    
                    return list(set(m_rel + p_rel))

                def get_father(self):
                    return self.has_father[0] if len(self.has_father) > 0 else self.has_father

                def get_mother(self):
                    return self.has_mother[0] if len(self.has_mother) > 0 else self.has_mother

                def get_relation_to_proband(self):
                    return self.relation_to_proband[0]

                def get_side_of_the_family(self):
                    if self.relation_to_proband[0] in ['MTH', 'MAUNT', 'MUNCLE', 'MGRMTH', 'MGRFTH']: return 'maternal'
                    elif self.relation_to_proband[0] in ['FTH', 'PAUNT', 'PUNCLE', 'PGRMTH', 'PGRFTH']: return 'paternal'
                    else: return 'unclear'

                def get_gene_mutations(self):
                    return self.has_mutation_in

                def get_cancers(self): 
                    return self.has_cancer

                def get_height(self):
                    return self.has_height
                
                def get_weight(self):
                    return self.has_weight

                #  expects weight in lbs and height in inches
                def get_bmi(self): #  healthy range is 18.5 to 24.9
                    if self.has_weight and self.has_height:
                        kg = self.has_weight / 2.2 
                        m = self.has_height * 0.0254  
                        bmi = kg / (m * m) 
                        return "{:0.1f}".format(bmi)
                    return "0"

                def get_relative_references(self, klass):
                    crit1_17_class_list = ['CRIT1.01', 'CRIT1.02', 'CRIT1.03', 'CRIT1.04', 'CRIT1.05', 'CRIT1.06', 'CRIT1.07', 'CRIT1.08', 'CRIT1.09', 'CRIT1.10', 'CRIT1.11', 'CRIT1.12', 'CRIT1.13', 'CRIT1.14', 'CRIT1.15']
                    references = []

                    if klass.name == 'ACMGAtRiskFDRPatient':
                        for fdr in self.has_mother + self.has_father + self.has_sibling:
                            if 'ACMGPatient' in [ cl.name for cl in fdr.get_class_list() ]:
                                ref = {
                                    'relation': fdr.get_relation_to_proband(),
                                    'name': fdr.get_patient_name(),
                                    'ontoName': fdr.name
                                }
                                references.append(ref)
                    if klass.name == 'CRIT1.17':
                        for rel in set(self.has_maternal_close_relative + self.has_paternal_close_relative):
                            if set(crit1_17_class_list) & set([ r.name for r in rel.get_class_list() ]):
                                ref = {
                                    'relation': rel.get_relation_to_proband(),
                                    'name': rel.get_patient_name(),
                                    'ontoName': rel.name
                                }
                                references.append(ref)

                    return references

                def get_patient_recommendations(self):
                    recommendations = []
                    if 'ACMGPatient' in self.get_class_list() and 'NCCNPatient' in self.get_class_list(): 
                        print("Reasoning may have failed to isolate worlds!")
                    if 'PatientWithRecommendation' in [cl.name for cl in self.get_class_list()]:
                        patient_criteria_classes = [ cl for cl in self.get_class_list() if cl.guideline and cl.recommendation_id ]
                        
                        sorted_patient_criteria = {}
                        for c in patient_criteria_classes:
                            rec_id = c.recommendation_id[0]
                            rec_text = c.recommendation_text[0]
                            composite_id = rec_id + '__' + rec_text
                            if composite_id not in sorted_patient_criteria:
                                sorted_patient_criteria[composite_id] = {
                                    'id': rec_id,
                                    'text': rec_text,
                                    'triggers': []
                                }
                            trigger = {
                                'id': c.name,
                                'criteria': c.criteria[0],
                                'source': c.guideline[0]
                            }

                            if c.syndrome:
                                trigger['syndrome'] = c.syndrome

                            if c.relative_risk:
                                trigger['references'] = self.get_relative_references(c)
                            sorted_patient_criteria[composite_id]['triggers'].append(trigger)

                        
                        for sc in sorted_patient_criteria:
                            recommendations.append(sorted_patient_criteria[sc])
                    return recommendations

                def is_cancer_patient(self):
                    return len(self.has_cancer) > 0

                def is_hereditary_cancer_risk_patient(self, guideline_assessment):
                    has_risk = False
                    for member in guideline_assessment:
                        if member['relation'] == 'self':
                            if len(member['recommendations']) > 0: return True
                    if self.hereditary_mutation_is_present():
                        return True
                    elif self.mutation_is_present():
                        return True
                    
                    return has_risk

                def is_deceased_patient(self):
                    return self.is_deceased

                def hereditary_mutation_is_present(self):
                    has_mutation_in_family = False
                    for member in self.get_relatives():
                        has_mutation_in_family = member.mutation_is_present()
                        if has_mutation_in_family: return has_mutation_in_family
                    return has_mutation_in_family

                def mutation_is_present(self):
                    return len(self.has_mutation_in) > 0

                def is_premenopausal(self):
                    return self.has_age_menopause == 0
