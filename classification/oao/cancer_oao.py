from owlready2 import *
from pprint import pprint

class CancerOao:
    def __init__(self, ontology_service):
        self.ontology_service = ontology_service
        self.onto = self.ontology_service.get_onto()
        self.build_cancer_class_def()

    def build_cancer_class_def(self):
        with self.onto:
            class Cancer(Thing):
                def get_age_onset(self):
                    return self.has_age_onset

                def get_hormone_statuses(self):
                    return [hs.display[0] for hs in self.has_hormone_status]
                
                def get_laterality(self):
                    return [l.display[0] for l in self.has_laterality]

                def get_cancer_tissue_origin(self):
                    return [cto.display[0] for cto in self.has_cancer_tissue_origin]

                def get_histology(self):
                    return [his.name for his in self.has_histology]

                def get_factors(self):
                    return [f.display[0] for f in self.has_factor]

                def get_cancer_display_name(self):
                    return self.display[0]
                
                def get_gleason_score(self):
                    if self.has_gleason_score: return self.has_gleason_score
                    return "unknown"