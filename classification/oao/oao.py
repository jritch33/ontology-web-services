from owlready2 import *

class Oao:
    def __init__(self, ontology_service):
        self.ontology_service = ontology_service
        self.onto = self.ontology_service.get_onto()
        self.build_thing_class_def()

    def build_thing_class_def(self):
        with owl:
            class Thing(owl.Thing):            
                def add_is_a(self, klass):
                    self.is_a.append(klass)
                    for a in klass.ancestors():
                        self.is_a.append(a)

                def has_super_class(self, klass):
                    return klass in self.get_class_list()

                def has_value(self, prop, val):
                    for ind in prop[self]:
                        if ind is val:
                            return True
                    return False

                def has_some(self, prop, val):
                    for ind in prop[self]: 
                        if val in ind.get_class_list(): return True
                    return False

                def get_cardinality_value_count(self, prop, classes):
                    ind_with_class = []
                    for value in prop[self]:
                        for klass in classes:
                            if klass in value.get_class_list(): ind_with_class.append(value)
                    return len(set(ind_with_class))

                def get_data_prop_value(self, prop):
                    if None not in prop[self]:
                        return prop[self][0]
                    return False
                
                def get_class_list(self):
                    classes = set()
                    for cl in self.is_a:
                        for a in cl.ancestors():
                            if a.name != 'Thing': classes.add(a)
                    return list(classes)
    
                def get_class(self):
                    return self.__class__

    def get_ancestry_instances(self):
        return self.onto.Ancestry.instances()

    def get_ancestry_instance(self, ancestry):
        for a in self.onto.Ancestry.instances():
            if a.name == ancestry:
                return a

        return add_ancestry_instance(ancestry)

    def add_ancestry_instance(self, ancestry):
        return self.onto.Ancestry(name=ancestry)

    def get_skin_color_instances(self):
        return self.onto.SkinColor.instances()

    def get_skin_color_instance(self, skin_color):
        for sk in self.onto.SkinColor.instances():
            if sk.name == skin_color:
                return sk

        return add_skin_color_instance(skin_color)

    def add_skin_color_instance(self, skin_color):
        return self.onto.SkinColor(name=skin_color)

    def get_concept_by_id(self, concept_id):
        return self.onto.search_one(id=concept_id)