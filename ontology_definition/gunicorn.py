import multiprocessing
import os

bind = '0.0.0.0:' + os.environ.get('PORT', '3000')
workers = multiprocessing.cpu_count() * 2 + 1
# workers = 1
timeout = 300
max_requests = 15