from owlready2 import * 

class HereditaryCancerIndividualService:
    def __init__(self, init_service, onto, df, restrict_to_core_concepts=False):
        self.init_service = init_service
        self.onto = onto
        self.df = df
        self.restrict_to_core_concepts = restrict_to_core_concepts

    def create_individuals(self):
        with self.onto:
            for klass in self.df['individuals']:
                for i in self.df['individuals'][klass]:
                    if self.restrict_to_core_concepts:
                        if self.onto[klass]:
                            self.onto[klass](name=i)
                    else: self.onto[klass](name=i)