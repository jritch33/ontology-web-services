from owlready2 import *
import os
import csv
import json

class HereditaryCancerAnnotationService:
    def __init__(self, init_service, onto, df, restrict_to_core_concepts=False):
        self.onto = onto
        self.df = df
        self.restrict_to_core_concepts = restrict_to_core_concepts

    def create_annotation(self, klass, annotations):
        for a in annotations:
            if self.onto[klass]: setattr(self.onto[klass], a, annotations[a])

    def add_dynamic_annotations(self, klass, class_obj):
        if 'annotations' not in class_obj:
            class_obj['annotations'] = {}
        if 'is_core_concept' in class_obj and class_obj['is_core_concept']:
            class_obj['annotations']['is_core_concept'] = True
            if 'display' not in class_obj['annotations']:
                class_obj['annotations']['display'] = klass

    def set_class_annotation_values(self, classes):
        for klass in classes:
            self.add_dynamic_annotations(klass, classes[klass])
            if 'annotations' in classes[klass]: self.create_annotation(klass, classes[klass]['annotations'])
            if 'subclasses' in classes[klass] and classes[klass]['subclasses']:
                self.set_class_annotation_values(classes[klass]['subclasses']) 

    def set_individual_annotation_values(self, individuals):
        for klass in individuals:
            for i in individuals[klass]:
                if 'annotations' in individuals[klass][i]:
                    self.create_annotation(i, individuals[klass][i]['annotations'])

    def create_annotations(self):
        with self.onto:
            self.set_class_annotation_values(self.df['classes'])
            self.set_individual_annotation_values(self.df['individuals'])