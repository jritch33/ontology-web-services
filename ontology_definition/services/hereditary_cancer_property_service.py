from owlready2 import *

class HereditaryCancerPropertyService:
    def __init__(self, init_service, onto, df, restrict_to_core_concepts=False):
        self.init_service = init_service
        self.onto = onto
        self.df = df
        self.data_types = {
            'int': int,
            'bool': bool
        }
        self.restrict_to_core_concepts = restrict_to_core_concepts

    def is_core_property(self, dom, ran, prop_type):
        dom_is_core = False
        ran_is_core = False
        if prop_type == 'object':
            for d in dom: 
                if self.onto[d]:
                    dom_is_core = True
                    break
            for r in ran:
                if self.onto[r]:
                    ran_is_core = True
                    break
        elif prop_type == 'data':
            for d in dom: 
                if self.onto[d]:
                    dom_is_core = True
                    break
            ran_is_core = True
        
        return dom_is_core and ran_is_core
        
    def create_property(self, dom, ran, prop, prop_type, prop_types):
        if prop_type == 'object':
            types.new_class(prop, (ObjectProperty,))
            for d in dom: self.onto[prop].domain.append(self.onto[d])
            for r in ran: self.onto[prop].range.append(self.onto[r])
        elif prop_type == 'data':
            types.new_class(prop, prop_types)
            for d in dom: self.onto[prop].domain.append(self.onto[d])
            for r in ran: 
                if r in self.data_types: self.onto[prop].range.append(self.data_types[r])
                else: self.onto[prop].range.append(self.onto[r])

    def create_annotation_property(self, properties):
        with self.onto:
            for prop in properties:
                types.new_class(prop, (AnnotationProperty,))

    def create_data_property(self, properties):
        with self.onto:
            for prop in properties:
                prop_types = (DataProperty,)
                if properties[prop]['isFunctional']: prop_types = prop_types + (FunctionalProperty,)
                dom = properties[prop]["domain"]
                ran = properties[prop]["range"]
                if self.restrict_to_core_concepts:
                    if self.is_core_property(dom, ran, 'data'):
                        self.create_property(dom, ran, prop, 'data', prop_types)
                else:
                    self.create_property(dom, ran, prop, 'data', prop_types)

    def create_object_property(self, properties):
        with self.onto:
            for prop in properties:
                dom = properties[prop]["domain"]
                ran = properties[prop]["range"]
                if self.restrict_to_core_concepts:
                    if self.is_core_property(dom, ran, 'object'):
                        self.create_property(dom, ran, prop, 'object', None)
                else:
                    self.create_property(dom, ran, prop, 'object', None)

    def create_properties(self):
        for prop in self.df['properties']:
            if prop == "ObjectProperty": self.create_object_property(self.df['properties'][prop])
            elif prop == "DataProperty": self.create_data_property(self.df['properties'][prop])
            elif prop == "AnnotationProperty": self.create_annotation_property(self.df['properties'][prop])