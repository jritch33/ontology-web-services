from owlready2 import *
import operator
import functools

class HereditaryCancerEquivalencyService:
    def __init__(self, init_service, onto, df, enable_equivalencies):
        self.init_service = init_service
        self.onto = onto
        self.df = df
        self.enable_equivalencies = enable_equivalencies
        self.mute_classes = []        

    def build_owl_class_operand(self, operand):
        return self.onto[operand['class']]
    
    def build_owl_value_operand(self, operand):
        if type(operand['value']) is bool : 
            return self.onto[operand['property']].value(operand['value'])
        
        return self.onto[operand['property']].value(self.onto[operand['value']])

    def build_owl_some_operand(self, operand):
        if 'and' in operand:
            return self.onto[operand['property']].some(self.build_operand_block(operand['and'], 'and'))
        elif 'or' in operand:
            return self.onto[operand['property']].some(self.build_operand_block(operand['or'], 'or'))
        return self.onto[operand['property']].some(self.build_owl_class_operand(operand))

    def build_owl_comparison_operand(self, operand):
        if operand['restriction'] == 'lt': return self.onto[operand['property']] < operand['value']
        if operand['restriction'] == 'lte': return self.onto[operand['property']] <= operand['value']
        if operand['restriction'] == 'gt': return self.onto[operand['property']] > operand['value']
        if operand['restriction'] == 'gte': return self.onto[operand['property']] >= operand['value']

    def build_owl_cardinality_operand(self, operand):
        if operand['restriction'] == 'min':
            if 'and' in operand:
                return self.onto[operand['property']].min(operand['cardinality'], self.build_operand_block(operand['and'], 'and'))
            elif 'or' in operand:
                return self.onto[operand['property']].min(operand['cardinality'], self.build_operand_block(operand['or'], 'or'))
            return self.onto[operand['property']].min(operand['cardinality'], self.build_owl_class_operand(operand))
        elif operand['restriction'] == 'max':
            if 'and' in operand:
                return self.onto[operand['property']].max(operand['cardinality'], self.build_operand_block(operand['and'], 'and'))
            elif 'or' in operand:
                return self.onto[operand['property']].max(operand['cardinality'], self.build_operand_block(operand['or'], 'or'))
            return self.onto[operand['property']].max(operand['cardinality'], self.build_owl_class_operand(operand))

    def build_owl_restriction_type_operand(self, operand):
        if operand['restriction'] == 'some':
            return self.build_owl_some_operand(operand)
        elif operand['restriction'] in ['lt', 'lte', 'gte', 'gt']:
            return self.build_owl_comparison_operand(operand)
        elif operand['restriction'] in ['min', 'max']:
            return self.build_owl_cardinality_operand(operand)

    def build_owl_prop_operand(self, operand):
        if operand['restriction'] == 'value':
            return self.build_owl_value_operand(operand)
        else:
            return self.build_owl_restriction_type_operand(operand)
    
    def build_owl_operand(self, operand):
        if 'property' in operand:
            return self.build_owl_prop_operand(operand)
        elif 'class' in operand: 
            return self.build_owl_class_operand(operand)
        elif 'and' in operand:
            return self.build_operand_block(operand['and'], 'and')
        elif 'or' in operand:
            return self.build_operand_block(operand['or'], 'or')
        elif 'not' in operand:
            return self.build_operand_block(operand['not'], 'not')
        else:
            print(operand)
            print("SOMETHING WENT WRONG")
            return operand

    def build_operand_block(self, operands, oper):
        owl_operands = ()
        for operand in operands:
            owl_operands = owl_operands + (self.build_owl_operand(operand),)
        if oper == 'and': return functools.reduce(operator.and_, owl_operands)
        elif oper == 'or': 
            return functools.reduce(operator.or_, owl_operands)
        elif oper == 'not': return (Not(owl_operands[0]))

    def create_equivalency(self, klass, eq_data):
        if 'property' in eq_data:
            self.onto[klass].equivalent_to = [self.build_owl_prop_operand(eq_data)]
        elif 'and' in eq_data:
            self.onto[klass].equivalent_to = [self.build_operand_block(eq_data['and'], 'and')]
        elif 'or' in eq_data:
            self.onto[klass].equivalent_to = [self.build_operand_block(eq_data['or'], 'or')]
        elif 'not' in eq_data:
            self.onto[klass].equivalent_to = [self.build_operand_block(eq_data['not'], 'not')]
        else:
            self.onto[klass].equivalent_to = [self.build_owl_operand(eq_data)]

    def skipEquivalencies(self, klass, klass_obj):
        # print(klass)
        # print(self.enable_equivalencies)
        if klass == 'ACMGPatient' and self.enable_equivalencies == 'acmg': 
            # print('skip ACMGPatient = False')
            klass_obj['skipEquivalencies'] = False
        if klass == 'NCCNPatient' and self.enable_equivalencies == 'acmg': 
            # print('skip NCCNPatient = True')
            klass_obj['skipEquivalencies'] = True

        if klass == 'ACMGPatient' and self.enable_equivalencies == 'nccn': 
            # print('skip ACMGPatient = True')
            klass_obj['skipEquivalencies'] = True
        if klass == 'NCCNPatient' and self.enable_equivalencies == 'nccn': 
            # print('skip NCCNPatient = False')
            klass_obj['skipEquivalencies'] = False

        if klass in self.mute_classes:
            klass_obj['skipEquivalencies'] = True

        if 'skipEquivalencies' in klass_obj:
            if klass_obj['skipEquivalencies'] == True: return True
        else: return False

        return False

    def create_class_equivalencies(self, classes):
        for klass in classes:
            if not self.skipEquivalencies(klass, classes[klass]):
                if 'equivalencies' in classes[klass]: 
                    self.create_equivalency(klass, classes[klass]['equivalencies'])
                if 'subclasses' in classes[klass] and classes[klass]['subclasses']:
                    self.create_class_equivalencies(classes[klass]['subclasses']) 

    def create_equivalencies(self):
        with self.onto:
            self.create_class_equivalencies(self.df['classes'])