from owlready2 import *
import json
import re
from io import *
from services.hereditary_cancer_class_hierarchy_service import HereditaryCancerClassHierarchyService
from services.hereditary_cancer_individual_service import HereditaryCancerIndividualService
from services.hereditary_cancer_property_service import HereditaryCancerPropertyService
from services.hereditary_cancer_equivalency_service import HereditaryCancerEquivalencyService
from services.hereditary_cancer_annotation_service import HereditaryCancerAnnotationService

class OntologyInitService():
    def __init__(self, onto_base_uri, onto_id, onto_spec, def_file=None, enable_equivalencies=None, restrict_to_core_concepts=False, is_dev=False):
        self.onto_id = onto_id
        self.onto_base_uri = onto_base_uri
        self.is_dev = is_dev   

        if self.is_dev: onto_path.append(os.getcwd() + '/repo_temp/')
        else: self.owl_file = BytesIO()

        self.def_file = def_file
        self.onto_spec = onto_spec
        self.ontology = get_ontology(self.onto_base_uri + self.onto_id + '_onto.owl')
        self.enable_equivalencies = enable_equivalencies
        self.restrict_to_core_concepts = restrict_to_core_concepts
        self.__initialize()

    def __initialize(self):
        print('Initializing ' + self.onto_id + ' ontology...')
        df = self.onto_spec

        print('Building class hierarchy...')
        hereditary_cancer_class_hierarchy_service = HereditaryCancerClassHierarchyService(self, self.ontology, df, self.restrict_to_core_concepts)
        hereditary_cancer_class_hierarchy_service.create_class_hierarchy()

        print('Creating individuals...')
        hereditary_cancer_individual_service = HereditaryCancerIndividualService(self, self.ontology, df, self.restrict_to_core_concepts)
        hereditary_cancer_individual_service.create_individuals()

        print('Creating properties...')
        hereditary_cancer_property_service = HereditaryCancerPropertyService(self, self.ontology, df, self.restrict_to_core_concepts)
        hereditary_cancer_property_service.create_properties()

        if not self.restrict_to_core_concepts:
            print('Creating equivalencies...')
            hereditary_cancer_equivalency_service = HereditaryCancerEquivalencyService(self, self.ontology, df, self.enable_equivalencies)
            hereditary_cancer_equivalency_service.create_equivalencies()
        else: print('Skipping equivalencies...')

        print('Saving annotations...')
        hereditary_cancer_annotation_service = HereditaryCancerAnnotationService(self, self.ontology, df, self.restrict_to_core_concepts)
        hereditary_cancer_annotation_service.create_annotations()  

        self.save_onto()
        
        print('Initialized.')
    
    def get_owl_str_obj(self):
        return self.owl_file.getvalue().decode('utf-8')
    
    def get_owl_byte_obj(self):
        return self.owl_file.getvalue()
    
    def get_onto_id(self):
        return self.onto_id

    def save_onto(self):
        if self.is_dev: self.ontology.save()
        else: self.ontology.save(self.owl_file)

    def reload(self):
        self.ontology.destroy()