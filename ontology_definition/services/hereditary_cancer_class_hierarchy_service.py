from owlready2 import *
import types

class HereditaryCancerClassHierarchyService:
    def __init__(self, init_service, onto, df, restrict_to_core_concepts=False):
        self.init_service = init_service
        self.onto = onto
        self.df = df
        self.restrict_to_core_concepts = restrict_to_core_concepts

    def is_core_concept(self, klass, classes):
        if 'is_core_concept' in classes[klass] and classes[klass]['is_core_concept']: 
            return True
        return False

    def create_class(self, klass, super_class):
        if super_class == 'Thing': types.new_class(klass, (Thing,))
        else: types.new_class(klass, (self.onto[super_class],))

    def create_class_recursive_call(self, klass, super_class, classes):
        self.create_class(klass, super_class)
        if 'subclasses' in classes[klass] and classes[klass]['subclasses']:
            self.create_classes(classes[klass]['subclasses'], klass) 

    def create_classes(self, classes, super_class):
        for klass in classes:
            if self.restrict_to_core_concepts: 
                if self.is_core_concept(klass, classes):
                    self.create_class_recursive_call(klass, super_class, classes)
            else:
                self.create_class_recursive_call(klass, super_class, classes)
                

    def set_disjoint_classes(self, disjoint_classes):
        for dc in disjoint_classes:
            dj_list = []
            if self.restrict_to_core_concepts:
                for d in dc:
                    if self.onto[d]: dj_list.append(self.onto[d])
            else:
                dj_list = [ self.onto[d] for d in dc ]
            AllDisjoint(dj_list)

    def create_class_hierarchy(self):
        with self.onto:
            self.create_classes(self.df['classes'], 'Thing')
            self.set_disjoint_classes(self.df['disjointClasses'])