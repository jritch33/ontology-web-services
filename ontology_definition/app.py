import re
import json
import jsonschema
import jsonref
from io import *
import inspect
from flask import Flask, Response, request
from flask_restplus import Api, Resource
from flask_swagger_ui import get_swaggerui_blueprint
from flask_expects_json import expects_json
from flask_cors import CORS
from collections import Counter
from owlready2 import *
from services.ontology_init_service import OntologyInitService
from pprint import pprint

with open('static/request-definitions.json') as rd:
    c = jsonref.load(rd)
    concept_schema = c['data']

api = Flask(__name__)
CORS(api)

swagger_url = '/swagger'
api_url = '/static/swagger.json'
swaggerui_blueprint = get_swaggerui_blueprint(
    swagger_url,
    api_url,
    config={
        'app_name': "Ontology Formalization API"
    }
)

api.register_blueprint(swaggerui_blueprint, url_prefix=swagger_url)

@api.route('/formalize', methods=['POST'])
@expects_json(concept_schema)
def formalize():
    if not request.get_json():
        abort(400)
    data = request.get_json(force=True)

    init_service = OntologyInitService(data['ontology_base_uri'], data['ontology_key'], data['conceptualization'])
    owl_str = init_service.get_owl_str_obj()
    init_service.reload()
    return owl_str, 200

@api.route('/formalizedev', methods=['POST'])
@expects_json(concept_schema['properties']['conceptualization'])
def formalize_dev():  
    if not request.get_json():
        abort(400)
    data = request.get_json(force=True)
    
    hc_init_service = OntologyInitService("http://oq/", 'hereditary_cancer', data, is_dev=True)
    hc_core_init_service = OntologyInitService("http://oq/", 'hereditary_cancer_core', data, restrict_to_core_concepts=True, is_dev=True)
    acmg_init_service = OntologyInitService("http://oq/", 'hereditary_cancer_acmg', data, enable_equivalencies='acmg', is_dev=True)
    nccn_init_service = OntologyInitService("http://oq/", 'hereditary_cancer_nccn', data, enable_equivalencies='nccn', is_dev=True)

    print('Generating core concepts file...')
    core_world = create_world('hereditary_cancer_core')
    generate_core_concepts_file(core_world)

    print('Checking for consistency...')
    world = create_world('hereditary_cancer')
    check_stats(world)
    return {}, 200

@api.route('/guidelines/<string:onto_id>')
def guidelines(onto_id):
    if not request.get_json():
        abort(400)
    data = request.get_json(force=True)

    init_service = OntologyInitService('hereditary_cancer', data)
    onto = get_ontology('https://oq.com/' + onto_id + '.owl').load(fileobj=init_service.get_owl_byte_obj())
    gl = list(onto.search(guideline="NCCN", criteria="*"))
    crit = [ c.criteria[0] for c in gl ]

    return crit, 200

def create_world(onto):
    world = World()
    return world.get_ontology('ontology_definition/repo_temp/' + onto + '_onto.owl').load()

def create_world_test(iri, owl_file):
    with open(os.getcwd() + '/repo_temp/hereditary_cancer_onto.owl', 'r') as f:
        world = World()
        return world.get_ontology(iri).load(fileobj = f)

def create_core_concept_object(c):
    concept_obj = {
        'id': c.id[0],
        'name': c.display[0]
    }

    if c.synonyms: concept_obj['keywords'] = c.synonyms

    subclasses = [sc for sc in c.subclasses()]
    if subclasses:
        subtypes = []
        for sc in subclasses:
            subtypes.append(create_core_concept_object(sc))
        concept_obj['subtypes'] = subtypes
    
    return concept_obj

def create_core_concept_objects(onto):
    core_concept_objs = []
    for c in Thing.subclasses(False, world=onto.world):
        if c.name in ['Cancer', 'CancerGene', 'Syndrome']: 
            core_concept_objs.append(create_core_concept_object(c))

    return core_concept_objs

def generate_core_concepts_file(onto):
    # check_old_codes(onto)
    core_concept_objects = create_core_concept_objects(onto)
    with open('data/core_concepts_test.json', 'w') as core_concept_file:
        json.dump(core_concept_objects, core_concept_file, ensure_ascii=False, indent=4)

def check_old_cancer_subtypes(subtypes, cancer_codes):
    old_cancer_codes = []
    for st in subtypes:
        if st['id'] not in cancer_codes:
            print(st['name'])
        if 'subtypes' in st:
            old_cancer_codes += [s['id'] for s in st['subtypes']]
            check_old_cancer_subtypes(st['subtypes'], cancer_codes)

    return old_cancer_codes

def all_concept_ids_unique(concept_ids):
    concept_counts = Counter(concept_ids)
    for k, v in concept_counts.items():
        if v > 1:
            print("DUPLICATE CONCEPT ID: ", k)

def natural_sort_key(s):
    _nsre = re.compile('([0-9]+)')
    return [int(text) if text.isdigit() else text.lower() for text in re.split(_nsre, s)]

def update_concept_id_file():
    with open(os.getcwd() + '/' + 'hereditary_cancer_definition_file.json') as df_file:
        df = json.load(df_file)
        string = json.dumps(df)
        ids = re.findall(r"IRF\d+", string)
        ids.sort(key=natural_sort_key)
    
    with open(os.path.dirname(os.path.abspath(__file__)) + '/data/hc_ontology_ids_in_use.json', 'w') as id_file:
        json.dump(ids, id_file, ensure_ascii=False, indent=4)

def check_stats(onto):
    with onto:
        concepts = Thing.descendants(include_self=False, world=onto.world)
        print(len(concepts)) 
        for c in concepts:
            if not c.id:
                print(c.name)
        concept_ids = [c.id[0] for c in concepts]
        all_concept_ids_unique(concept_ids)
        update_concept_id_file()

def check_old_codes(onto):
    onto = onto.get_onto()
    with open('codes.json', 'r') as f:
        data = json.load(f)

        with onto:
            for item in data:
                if item['id'] == 'Gene':
                    for st in item['subtypes']:
                        if not onto[st['name']]:
                            print(st['name'])
            
            cancer_codes = [c.id[0] for c in onto['Cancer'].descendants() if c.id and c.is_core_concept]
            cancers_without_codes = [c.name for c in onto['Cancer'].descendants() if not c.id and c.is_core_concept]
            print(cancers_without_codes)

if __name__ == '__main__':
    api.run(debug=True)
    