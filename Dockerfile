FROM ubuntu:20.10

LABEL maintainer="Jordon Ritchie <ritchiej@musc.edu>"

ENV DEBIAN_FRONTEND=noninteractive 
RUN apt-get update && apt-get install -y \
  netcat \
  python3.7 \
  python3-pip \
  python3-dev \
  software-properties-common \
  openjdk-8-jdk \
    # add-apt-repository ppa:webupd8team/java -y &&  \
    # apt-get update && \
    #accepts the license
    # echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \ 
    # apt-get install -y oracle-java8-installer && \
  && apt-get clean

# RUN update-alternatives --install /usr/bin/python3 python /usr/bin/python3.7 10
# RUN add-apt-repository ppa:jonathonf/python-3.7

COPY ./ ./app
WORKDIR ./app

# RUN pip3 install -r requirements.txt
RUN pip3 install pipenv
RUN pipenv install --system

# COPY ./nginx.conf /etc/nginx/sites-enabled/default

HEALTHCHECK --interval=5m --timeout=5s \
  CMD nc -z localhost $PORT

CMD gunicorn -c gunicorn.py classification.app:api
# CMD service nginx start && uwsgi -s /tmp/uwsgi.sock --chmod-socket=666 --manage-script-name --mount /=app:app

